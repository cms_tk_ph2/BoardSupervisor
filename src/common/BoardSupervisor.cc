/* Copyright 2014 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		BoardSupervisor.cc
   Content : 		Supervisor module
   Programmer : 	Christian Bonnin
   Version : 		
   Date of last modification : 01/07/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/

#include <exception>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/format.hpp>
#include "System/SystemController.h"
#include "BoardSupervisor.h"
#include "FedEvent.h"
#include "Utils/easylogging++.h"
INITIALIZE_EASYLOGGINGPP

XDAQ_INSTANTIATOR_IMPL(BoardSupervisor::Supervisor)

using namespace cgicc;
using namespace std;

BoardSupervisor::Supervisor::Supervisor(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception): xdaq::WebApplication(s),
fsm_(this), 
i2cComm("cbc_reg_i2c_settings", "cbc_reg_i2c_command", "cbc_reg_i2c_reply", 0x5B),
streamer(this)
{
    gFedDebugString_="Supervisor";
    
    /* Give funny and useless informations at load time */
    std::stringstream mmesg;
    mmesg << "Process version " << SUPERVISOR_PACKAGE_VERSION << " loaded";
    this->getApplicationLogger().setLogLevel(INFO_LOG_LEVEL);
    LOG4CPLUS_INFO(this->getApplicationLogger(), mmesg.str());
    this->getApplicationLogger().setLogLevel(ERROR_LOG_LEVEL);
    //uhal::setLogLevelTo ( uhal::Warning() );

    /* bind xgi and xoap commands specific to this application */
//    xgi::bind(this,&Supervisor::ParameterQuery, "ParameterQuery");
    xgi::bind(this,&Supervisor::Default, "Default");
    xgi::bind(this,&Supervisor::MainPage, "mainPage");
    xgi::bind(this,&Supervisor::FsmTransition, "fsmTransition");
    xgi::bind(this,&Supervisor::FsmDiagram, "fsmDiagram");
    xgi::bind(this,&Supervisor::AcquisitionParam, "acquisition");
    xgi::bind(this,&Supervisor::FpgaConfig, "fpga");
    xgi::bind(this,&Supervisor::StreamerAcq, "streamer");
	xgi::bind(this,&Supervisor::SaveParam, "saveParameters");    
	xgi::bind(this,&Supervisor::LoadParam, "loadParameters");
	xgi::bind(this,&Supervisor::SaveSetup, "saveSetup");    
	xgi::bind(this,&Supervisor::LoadSetup, "loadSetup");
	xgi::bind(this,&Supervisor::SaveFeCbcEnabling, "saveFeCbcEnabling");    
//	xgi::bind(this,&Supervisor::LoadFeCbcEnabling, "loadFeCbcEnabling");
	xgi::bind(this,&Supervisor::ConfigureBoard, "configureBoard");
	xgi::bind(this,&Supervisor::I2cReset, "i2cReset");
	xgi::bind(this,&Supervisor::I2cWrite, "i2cWrite3");
	xgi::bind(this,&Supervisor::I2cRead, "i2cRead");
	xgi::bind(this,&Supervisor::I2cWriteFileValues, "i2cWriteFileValues");
	xgi::bind(this,&Supervisor::I2cWriteOneValue, "i2cWriteOneValue");
	xgi::bind(this,&Supervisor::I2cDefaultToMem, "i2cDefaultToMem");
	xgi::bind(this,&Supervisor::I2cCheckAllValues, "i2cCheckAllValues");
	xgi::bind(this,&Supervisor::I2cSaveToFile, "i2cSaveToFile");
	xgi::bind(this,&Supervisor::CbcHardReset, "cbcHardReset");
	xgi::bind(this,&Supervisor::CbcReset101, "cbcReset101");
	xgi::bind(this,&Supervisor::Test, "test");
    xgi::bind(this,&Supervisor::LoadFromFileHtml, "loadHtmlValues");
    xgi::bind(this,&Supervisor::SaveToFileHtml, "saveHtmlValues");
    xgi::bind(this,&Supervisor::LoadSetupFromFile, "loadSetupValues");
    xgi::bind(this,&Supervisor::SaveSetupToFile, "saveSetupValues");
    xgi::bind(this,&Supervisor::RebootBoard, "rebootBoard");
    xgi::bind(this,&Supervisor::UploadImage, "uploadImage");

    
    // Make variables available in the application InfoSpace
    this->getApplicationInfoSpace()->fireItemAvailable("HWDescription", &fileHwDescription);
    this->getApplicationInfoSpace()->fireItemAvailable("HWDescriptionI2c", &fileHwDescriptionI2c);
    this->getApplicationInfoSpace()->fireItemAvailable("StartupSequence", &strInitSequence);
    this->getApplicationInfoSpace()->fireItemAvailable("ParamHtmlFile", &fileParamHtml);
    this->getApplicationInfoSpace()->fireItemAvailable("SetupHtmlFile", &fileSetupHtml);
    this->getApplicationInfoSpace()->fireItemAvailable("FpgaHtmlFile", &fileFpgaHtml);
    getApplicationInfoSpace()->fireItemAvailable("StreamerParamHtmlFile", &streamer.fileParamHtml);
    getApplicationInfoSpace()->fireItemAvailable("ConditionHtmlFile", &streamer.fileConditionHtml);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_FAKE_DATA, &bPlaybackMode);
    acquisition.getStreamerParameterSet().mapToSerializable(this, "RU",&streamer.intRuInstance);
    acquisition.getStreamerParameterSet().mapToSerializable(this, "RUName",&streamer.strRuName);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_SHORT_PAUSE, &streamer.intShortPause);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_LONG_PAUSE, &streamer.intLongPause);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_USE_HARDWARE_COUNTER, &streamer.bHardwareCounter);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_SHARED_MEMORY, &streamer.bSharedMemory);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_MEMTOFILE,&streamer.bMemToFile);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_BREAK_TRIGGER,&streamer.bBreakTrigger);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_NB_ACQ,&streamer.intNbAcq);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DISPLAY_LOG,&streamer.bDisplayLog);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DISPLAY_FLAGS,&streamer.bDisplayFlags);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DISPLAY_DATA_FLAGS,&streamer.bDataFlags);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DISPLAY_COUNTERS,&streamer.bDisplayCounters);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_ACQ_MODE,&streamer.intAcqMode);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DATA_FILE,&streamer.strDataFile);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DEST_FILE,&streamer.strDestFile);
    //acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DEST_MAX_SIZE,&streamer.intDestMaxSize);
    //acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_2ND_DEST_FILE,&streamer.str2ndDestFile);
    //acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_2ND_DEST_RATIO,&streamer.int2ndDestRatio);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_NEW_DAQ_FILE,&streamer.strNewDaqFile);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_ZERO_SUPPRESSED,&streamer.bZeroSuppressed);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_CONDITION_DATA,&streamer.bConditionData);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_COMMISSIONING_LOOP,&streamer.bCommissionningLoop);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_ACF_ACQUISITION,&streamer.intAcfAcquisition);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_READ_NEXT_FILES,&streamer.bReadNextFiles);
    acquisition.getStreamerParameterSet().mapToSerializable(this, STREAMER_DATA_VISU,&streamer.intDataVisu);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DATASIZE, &intDataSize);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_COMMISSIONNING_DELAY_AFTER_FAST_RESET, &intCommissionningDelayAfterFastReset);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_COMMISSIONNING_DELAY_AFTER_L1A, &intCommissionningDelayAfterL1a);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_COMMISSIONNING_DELAY_AFTER_TEST_PULSE, &intCommissionningDelayAfterTestPulse);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_COMMISSIONNING_RQ, &intCommissionningRequest);
    acquisition.getParameterSet().mapToSerializable(this, "cbc.STUBDATA_LATENCY_ADJUST", &intStubLatencyAdjust);
    acquisition.getParameterSet().mapToSerializable(this, "cbc.STUBDATA_LATENCY_MODE", &intStubLatencyMode);
    acquisition.getParameterSet().mapToSerializable(this, "cbc_i2c_clk_prescaler", &intI2cClkPrescaler);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_CLK_MUX_SEL, &intClkMuxSel);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_BACKPRESSURE_OUT_50OHMS, &intBackpressureOut50Ohms);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_BACKPRESSURE_OUT_POLAR, &intBackpressureOutPolar);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_CLK_IN_50OHMS, &intClkIn50Ohms);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_CLK_OUT_50OHMS, &intClkOut50Ohms);
    acquisition.getParameterSet().mapToSerializable(this, "dio5.fmcdio5_lemo2_sig_sel", &intLemo2SigSel);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_THRESHOLD_CLK_IN, &intThresholdClkIn);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_THRESHOLD_TRIG_IN, &intThresholdTrigIn);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_TRIG_IN_50OHMS, &intTrigIn50Ohms);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_TRIG_IN_EDGE, &intTrigInEdge);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_DIO5_TRIG_OUT_50OHMS, &intTrigOut50Ohms);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_EXTERNAL_DATA, &intExternalData);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_TRIGGER_FREQ, &intTriggerFreq);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_TRIGGER_FROM_TTC, &intTriggerFromTtc);
    acquisition.getParameterSet().mapToSerializable(this, "pc_commands.readoutDataType", &intReadoutDataType);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_FE0_MASKED, &intFe0Masked);
    acquisition.getParameterSet().mapToSerializable(this, PARAM_FE1_MASKED, &intFe1Masked);
    acquisition.getParameterSet().mapToSerializable(this, "readoutReleaseDelay", &intReadoutReleaseDelay);
    acquisition.getParameterSet().mapToSerializable(this, "pc_commands.readoutRelease", &intReadoutRelease);
    //getApplicationInfoSpace()->fireItemAvailable("ConditionKey", &streamer.vecConditionKey);
    //getApplicationInfoSpace()->fireItemAvailable("ConditionValue", &streamer.vecConditionValue);
    acquisition.getConditionParameterSet().mapToSerializable(this, "ConditionKey", &streamer.vecConditionKey);
    acquisition.getConditionParameterSet().mapToSerializable(this, "ConditionValue", &streamer.vecConditionValue);
    // Detect when the setting of default parameters has been performed
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    fsm_.initialize<BoardSupervisor::Supervisor>(this);
    iCurrentPage=VIEW_TYPE_MAIN;
}


BoardSupervisor::Supervisor::~Supervisor() {
}

///xdata::ActionListener method called when configuration values are set
void BoardSupervisor::Supervisor::actionPerformed (xdata::Event& e) {
        if (e.type() == "urn:xdaq-event:setDefaultValues") {
	    bool bStartAction=false, bAutoExit=false;
            try {
        	string strSequence = boost::algorithm::to_lower_copy(strInitSequence.toString());
		if (strSequence.find("init")!=string::npos){
			cout<<"Startup sequence: initialisation"<<endl;
			fsm_.fireEvent("Initialise", this);
			int iTime=0, TIMEOUT=10;
			if (strSequence.find("config",4)!=string::npos){
				while (fsm_.getCurrentState()!='H' && iTime<TIMEOUT){
					iTime++;
					sleep(1);
				}
				if (fsm_.getCurrentState()=='H'){//Halted
					cout<<"Startup sequence: configuration"<<endl;
					fsm_.fireEvent("Configure", this);
					iTime=0;
					if (strSequence.find("i2c",10)!=string::npos){
						while (fsm_.getCurrentState()!='C' && iTime<TIMEOUT){
							iTime++;
							sleep(1);
						}
						if (fsm_.getCurrentState()=='C'){//Configured
							cout<<"Startup sequence: I2C write"<<endl;
							I2cWriteFileValues(NULL, NULL);
						} else
							cout<<"Startup sequence: TIME OUT!"<<endl; 
					}
					if (strSequence.find("start",10)!=string::npos || strSequence.find("enable",10)!=string::npos){
						bAutoExit=strSequence.find("quit")!=string::npos;
						while (fsm_.getCurrentState()!='C' && iTime<TIMEOUT){
							iTime++;
							sleep(1);
						}
						if (fsm_.getCurrentState()=='C'){//Configured
							bStartAction=true;
						} else
							cout<<"Startup sequence: TIME OUT!"<<endl; 
					}
				} else
					cout<<"Startup sequence: TIME OUT!"<<endl; 
			}
		}
            } catch(std::exception& e) {
                LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
            }
	    streamer.actionPerformed(e);
	    if (bPlaybackMode)
		iCurrentPage=VIEW_TYPE_ACQUISITION;

	    if (bStartAction){
		    cout<<"Startup sequence: start"<<endl;
		    fsm_.fireEvent("Enable", this);
		    acquisition.setAutoExit(bAutoExit);
	    }

        }
}
/* Default() hyperDaq method */
void BoardSupervisor::Supervisor::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    createHtmlHeader(out, iCurrentPage);

    WebShowStateMachine(out);
    if (iCurrentPage==VIEW_TYPE_MAIN)
	    insertDescription(out);
    else
	    streamer.Default(in, out);

    /* Create HTML footer */
    //xgi::Utils::getPageFooter(*out);
    iAutoRefresh=0;
}

void BoardSupervisor::Supervisor::MainPage(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	iCurrentPage=VIEW_TYPE_MAIN;
	Default(in, out);
}

/* Create HTML header */
void BoardSupervisor::Supervisor::createHtmlHeader(xgi::Output *out, int iViewType, const std::string& strDest) {
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << "<html lang='en' dir='ltr' >"<<endl;//cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
    if (iAutoRefresh){
        *out << cgicc::meta().set("HTTP-EQUIV", "REFRESH")
        	.set("CONTENT", toolbox::toString("%d; url=%s/%s/%s",iAutoRefresh, 
        			getApplicationDescriptor()->getContextDescriptor()->getURL().c_str(),
        			getApplicationDescriptor()->getURN().c_str(), strDest.c_str()
        			)) << endl;
    }
    	
    *out << cgicc::title("BoardSupervisor") << std::endl;
    string strHeader;

    switch (iViewType){
	    case VIEW_TYPE_MAIN: strHeader="Main - <a href='acquisition'>Parameters</a> - <a href='i2cRead'>I2C values</a> - <a href='fpga'>Firmware</a> - <a href='streamer'>Acquisition</a>";break;
	    case VIEW_TYPE_PARAM: strHeader="<a href='mainPage'>Main</a> - Parameters - <a href='i2cRead'>I2C values</a> - <a href='fpga'>Firmware</a> - <a href='streamer'>Acquisition</a>";break;
	    case VIEW_TYPE_I2C: strHeader="<a href='mainPage'>Main</a> - <a href='acquisition'>Parameters</a> - I2C values - <a href='fpga'>Firmware</a> - <a href='streamer'>Acquisition</a>";break;
	    case VIEW_TYPE_FPGA: strHeader="<a href='mainPage'>Main</a> - <a href='acquisition'>Parameters</a> - <a href='i2cRead'>I2C values</a> - Firmware - <a href='streamer'>Acquisition</a>";break;
	    case VIEW_TYPE_ACQUISITION: strHeader="<a href='mainPage'>Main</a> - <a href='acquisition'>Parameters</a> - <a href='i2cRead'>I2C values</a> - <a href='fpga'>Firmware</a> - Acquisition";break;
	    default /*VIEW_TYPE_DIAGRAM*/: strHeader="<a href='mainPage'>Main</a> - <a href='acquisition'>Parameters</a> - <a href='i2cRead'>I2C values</a> - <a href='fpga'>Firmware</a> - <a href='streamer'>Acquisition</a>";break;
    }
    	
    xgi::Utils::getPageHeader(  out, "BoardSupervisor", strHeader, getApplicationDescriptor()->getContextDescriptor()->getURL(), 	
				getApplicationDescriptor()->getURN(), "/xgi/images/Application.jpg" );
}

void BoardSupervisor::Supervisor::insertDescription(xgi::Output * out) {
//    int height=50, width=70;
//    *out<<"<table border='1'><tr><td height='"<< height*3<<"' width='"<<4*width<<"' colspan='4'>";
//    *out<<mainBoard.getFmc1Description();
//    *out<<"</td>\n<td border='0' width='"<<4*width<<"' colspan='4'>"<<mainBoard.getFmc2Description();
//    *out<<"</td></tr>\n";
//    *out<<"<tr><td width='"<<2*width<<"' height='"<<2*height<<"' colspan='3'>Board ID:<br>Sys ID:<br>Firmware ID:</td>\n";
//    *out<<"<td height='"<<2*height<<"' width='"<<2*width<<"' colspan='2'>";
//    *out<<mainBoard.getFirmwareDescription();
//    *out<<"</td>\n<td height='"<<3*height<<"' width='"<<3*width<<"' rowspan='2' colspan='3' align='center'>";
//    *out<<mainBoard.getGbtDescription()<<"</td></tr>";
//    *out<<"<tr><td height='"<<height<<"' width='"<<5*width<<"' rowspan='1' colspan='5'> </td></tr>\n";
//    *out<<"</table>"<<endl;
    *out<<br()<<endl;
    fBoardMutex.lock();

    if (mainBoard.getBeBoardInterface()){
	*out<<acquisition.getConfigParameterSet().transformHtmlFileDescription(fileSetupHtml.toString(),"");//, "SetupValues");
	if (bPlaybackMode)
	   *out<<h3("Playback mode: no board description.")<<endl;
	else {
	    *out<<form(input().set("type","submit").set("value","Load setup from board")).set("action","loadSetup")<<endl;
	    mainBoard.insertFeCbcEnabling(out, acquisition.getConfigParameterSet(), acquisition.getFeCbcParameterSet());
	    *out<<hr()<<table();
	    //*out<<tr()<<td("CBC I<sup>2</sup>C bus:");
	    //*out<<form().set("method", "POST").set("action","i2cRead")<<td(input().set("type","submit").set("value","I2C values") .set("name","readAction").set("title","Go to I2C values management page using the following directory"));
	//    *out<<td(input().set("type","submit").set("value","Write I2C").set("name","writeAction"));
	    //*out<<td(input().set("type","text").set("size","50").set("name","i2cFiles").set("value",i2cComm.getValuesDirectory()).  set("title","Directory containing I2C values files"))<<form()<<tr();
	    *out<<tr()<<td("CBC reset:");
	    *out<<td(form(input().set("type","submit").set("value","Hard reset")).set("action","cbcHardReset"));
	    *out<<td(form(input().set("type","submit").set("value","Fast reset")).set("action","cbcReset101"));
	    *out<<tr()<<endl;
	    if (mainBoard.getBeBoard()->getBoardType()=="GLIB") {
		    *out<<tr()<<td("GLIB I<sup>2</sup>C bus:")<<td(form(input().set("type","submit").set("value","Reset")).set("action","i2cReset"));
		    *out<<td(form(input().set("type","submit").set("value","Write 3 values")).set("action","i2cWrite3"))<<tr()<<endl;
	    }
	    *out<<tr()<<form().set("method", "POST").set("action","test")<<td(input().set("type","text").set("size","20").set("name","testEdit").set("value","10000"));
	    *out<<td(input().set("type","submit").set("value","test"))<<form()<<tr()<<endl;
	    *out<<table()<<br()<<endl;
	}
    } 

    fBoardMutex.unlock();
//    return (lFmc1.valid() && lFmc2.valid());
}

/// Insert links for all CBCs, and I2C bus values in an HTML table for the current CBC
void BoardSupervisor::Supervisor::insertI2cDescription(xgi::Output * out, bool bRead) {
    if (mainBoard.getBeBoardInterface()){
	    	if (bPlaybackMode)
		   *out<<h3("Playback mode, no CBC configuration.")<<endl;
		else if (fsm_.getCurrentState()=='H' )//Halted
			*out<<h3("The board has to be configured")<<form(input().set("type","submit").set("name","transition").set("value","Configure") )
				.set("method","get").set("action",string("/")+getApplicationDescriptor()->getURN()+"/fsmTransition")<<endl;
		else try{
			*out<<h3("I2C values")<<endl;
			*out<<"Directory of I2C values files: "<<mainBoard.getI2cDirectory()<<small(" (they are read at initialisation)")<<br()<<br()<<endl;
			*out<<"<table><tr><td valign='top'>";
			*out<<"Check CBC chips to be configured and click Write<br/> to send the last column values into the board.";
			*out<<form().set("method","GET").set("action","i2cWriteFileValues")<<endl;
//			*out<<mainBoard.getFeAndCbcDescription()<<endl;
			mainBoard.insertFeCbcLinks(out, acquisition.getConfigParameterSet(), acquisition.getFeCbcParameterSet());
			*out<<"Verify writing"<<input().set("type", "checkbox").set("name", "checkWriting")<<" "<<endl;
			*out<<form()<<endl;
			*out<<form().set("method","GET").set("action","i2cCheckAllValues").set("name","frmCheckAll");
			*out<<input().set("type","hidden").set("name","checkedBox");
			*out<<input().set("type","submit").set("value","Check all values").set("onclick", "document.frmCheckAll.checkedBox.value=getCheckedBoxes();").set("title","Read all values from board and compare them to the ones stored in memory");
			*out<<form()<<endl;
			*out<<"</td>"<<endl;
			*out<< td(i2cComm.showValuesFromI2cAndFile(pI2cBoard->getCbcInterface(), pI2cBoard->getBeBoard(), bRead)).set("align","center");
			*out<<"</tr></table>"<<endl;
			*out<<a("Main").set("href",".")<<" - "<<a("Parameters").set("href","acquisition")<<" - "<<a("Firmware").set("href","fpga")<<endl;

			*out<<"<script> function getCheckedBoxes(){"<<endl;
			*out<<" var strRet=''; lst=document.getElementsByTagName('input');"<<endl;
			*out<<" for (var i=0; i<lst.length; i++){	if (lst[i].name.slice(0,5)=='chkFE' && lst[i].checked){"<<endl;
			*out<<"    if (strRet.length>0) strRet+=' ';"<<endl;
			*out<<"    strRet+=lst[i].name.slice(3);"<<endl;
			*out<<" } }"<<endl;
			*out<<" return strRet; }"<<endl;
			*out<<"</script>"<<endl;
	 	} catch(std::exception& e) {
			LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
			*out<<br()<<"Error while reading file: "<<i2cComm.getValuesFile()<<endl<<e.what();
	    }
    } else 
		*out<<h3("Error while reading from card: not initialised");
}
/// Perform a transition in the Finite State Machine
void BoardSupervisor::Supervisor::FsmTransition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    try {
        cgicc::Cgicc cgi(in);
	string strTrans=cgi["transition"]->getValue();
        fsm_.fireEvent(strTrans, this);
        iAutoRefresh=(strTrans=="Configure"  ? 3 : 1);
        Default(in, out);
    } catch(const std::exception& e) {
        XCEPT_RAISE(xgi::exception::Exception,  gFedDebugString_+e.what());
    }
}
/// Show the Finite State Machine Diagram
void BoardSupervisor::Supervisor::FsmDiagram(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    createHtmlHeader(out, VIEW_TYPE_DIAGRAM);
    *out << img().set("src", "/img/xdaqFsm.png")<<endl;
    //xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Supervisor::AcquisitionParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    iAutoRefresh=0;
    createHtmlHeader(out, VIEW_TYPE_PARAM);
    WebShowStateMachine(out);
    if (mainBoard.getBeBoardInterface()){
	    if (bPlaybackMode){
		    *out<<h3("Playback mode, no acquisition parameter.")<<endl;
		    return;
	    }
	    *out<<form(input().set("type","submit").set("value","Load from board")).set("action","loadParameters")<<endl;
	    *out<<acquisition.getParameterSet().transformHtmlFileDescription(fileParamHtml.toString(), "HtmlValues");
    } else
    	*out<<strong("Error: board not initialized!");
    
    /* Create HTML footer */
    //xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Supervisor::StreamerAcq(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    iCurrentPage=VIEW_TYPE_ACQUISITION;
    createHtmlHeader(out, VIEW_TYPE_ACQUISITION);
    iAutoRefresh=0;
    WebShowStateMachine(out);
    streamer.Default(in, out);
}

void BoardSupervisor::Supervisor::SaveParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	
	acquisition.getParameterSet().readAllValuesAndWriteIntoBoard(&cgi, false);
	AcquisitionParam(in, out);
}

void BoardSupervisor::Supervisor::LoadParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	if (mainBoard.getBeBoardInterface()){
		acquisition.getParameterSet().readAllFromBoard();
		acquisition.initConfiguration();
		AcquisitionParam(in, out);
	} else
		*out<<h3("Error while reading from card: not initialised");
}
///Write setup values into board
void BoardSupervisor::Supervisor::SaveSetup(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	
	acquisition.getConfigParameterSet().readAllValuesAndWriteIntoBoard(&cgi, false);
	acquisition.initFeCbcParameterSet();
	Default(in, out);
}
///Load setup values from board
void BoardSupervisor::Supervisor::LoadSetup(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	if (mainBoard.getBeBoardInterface()){
//		acquisition.getConfigParameterSet().readAllFromBoard();//temporary
//		acquisition.initFeCbcParameterSet();
		acquisition.getFeCbcParameterSet().readAllFromBoard();
		Default(in, out);
	} else
		*out<<h3("Error while reading from card: not initialised");
}

void BoardSupervisor::Supervisor::SaveFeCbcEnabling(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	
	acquisition.getFeCbcParameterSet().readAllValuesAndWriteIntoBoard(&cgi, false);
	Default(in, out);
}

void BoardSupervisor::Supervisor::ConfigureBoard(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    try {
        fsm_.fireEvent("Configure", this);
        Default(in, out);
    } catch(const std::exception& e) {
        XCEPT_RAISE(xgi::exception::Exception,  gFedDebugString_+e.what());
    }
}

void BoardSupervisor::Supervisor::I2cReset(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	I2cCommunication i2cOld("i2c_settings","i2c_command", "i2c_reply", 0x40);

    createHtmlHeader(out, VIEW_TYPE_MAIN);
    WebShowStateMachine(out);
    insertDescription(out);
    if (!mainBoard.getBeBoardInterface())
		*out<<h3("Error while reading from card: not initialised");
    else if (i2cOld.reset(mainBoard.getBeBoardInterface(), mainBoard.getBeBoard()))
        *out<< "Reset done<br/>";
    else
        *out<< "RESET ERROR !<br/>";

    *out<<pre(i2cOld.getLog())<<br()<<endl;
	
    /* Create HTML footer */
    //xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Supervisor::I2cWrite(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	I2cCommunication i2cOld("i2c_settings","i2c_command", "i2c_reply", 0x40);

    createHtmlHeader(out, VIEW_TYPE_MAIN);
    WebShowStateMachine(out);
    insertDescription(out);
    if (!mainBoard.getBeBoardInterface())
		*out<<h3("Error while reading from card: not initialised");
    else if (i2cOld.write3values(mainBoard.getBeBoardInterface(), mainBoard.getBeBoard()))
        *out<< "Write done<br/>";
    else
        *out<< "Write ERROR !<br/>";

    *out<<pre(i2cOld.getLog())<<br()<<endl;
	
    /* Create HTML footer */
    //xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Supervisor::I2cRead(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    cgicc::Cgicc cgi(in);
    iAutoRefresh=0;
    createHtmlHeader(out, VIEW_TYPE_I2C);
	
	//if (cgi.getElement("i2cFiles") != cgi.getElements().end())//File edit zone not empty
		//i2cComm.setValuesDirectory(cgi["i2cFiles"]->getValue());
		
	if (i2cComm.getValuesDirectory().length())
	   try{    
		   i2cComm.setValuesFile(cgi.getElement("cbc") != cgi.getElements().end() ? cgi["cbc"]->getValue() : "");
		   insertI2cDescription(out, cgi.queryCheckbox("read"));
	   } catch(std::exception& e) {
		   LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
	   }

    /* Create HTML footer */
    //xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Supervisor::I2cWriteFileValues(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	
    if (out)
	createHtmlHeader(out, VIEW_TYPE_I2C);

    mainBoard.initCbcFilename();
    string strFilename=mainBoard.nextCbcFilename(acquisition.getFeCbcParameterSet());
    string strChk = "chk";
    while (!strFilename.empty()){
	    if (in==NULL || cgi.queryCheckbox(strChk+strFilename)){
		    i2cComm.setValuesFile(strFilename);
		    if (i2cComm.writeFileValues(pI2cBoard->getCbcInterface(), pI2cBoard->getBeBoard(), 
				in==NULL ? (strInitSequence.toString().find("check")!=string::npos) : cgi.queryCheckbox("checkWriting"))){
			if (out)
				*out<< "Write done:"<<strFilename<<"<br/>"<<endl;
			else
				cout<< "Write done:"<<strFilename<<endl;
		    } else {
			if (out)	
				*out<< "Write ERROR !<br/>"<<pre(i2cComm.getLog())<<br()<<endl;
			else
				cout<< "Write ERROR !"<<i2cComm.getLog()<<endl;

		        break;
		    }
		}
		strFilename=mainBoard.nextCbcFilename( acquisition.getFeCbcParameterSet());
//		if (!strFilename.empty())			sleep(2);
    }
    i2cComm.setValuesFile("");
    if (out) 
	insertI2cDescription(out, false);	
    //xgi::Utils::getPageFooter(*out);
}
/// Write one value into the checked CBCs or the current one if none is checked
void BoardSupervisor::Supervisor::I2cWriteOneValue(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    cgicc::Cgicc cgi(in);
	
    createHtmlHeader(out, VIEW_TYPE_I2C);
    i2cComm.writeOneCbcValue(pI2cBoard->getCbcInterface(), pI2cBoard->getBeBoard(), cgi["regName"]->getValue(), cgi["regValue"]->getValue(), 
				cgi["checkedBox"]->getValue());
    *out<<pre(i2cComm.getLog())<<endl;
    insertI2cDescription(out, false);	
}
/// Set default values of the current CBC into its memory values
void BoardSupervisor::Supervisor::I2cDefaultToMem(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    cgicc::Cgicc cgi(in);
    createHtmlHeader(out, VIEW_TYPE_I2C);
    try{    
            i2cComm.setValuesFile(cgi.getElement("cbc") != cgi.getElements().end() ? cgi["cbc"]->getValue() : "");
            i2cComm.setDefaultValuesIntoMem(pI2cBoard->getBeBoard());
            insertI2cDescription(out, false);
    } catch(std::exception& e) {
		   LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    }
}
/// Read all values from the checked CBCs or the current one if none is checked and compare them with the memory values
void BoardSupervisor::Supervisor::I2cCheckAllValues(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    cgicc::Cgicc cgi(in);
    createHtmlHeader(out, VIEW_TYPE_I2C);
    *out<<h3("I2C values check results")<<endl;
    i2cComm.checkAllValues(pI2cBoard->getCbcInterface(), pI2cBoard->getBeBoard(),  cgi["checkedBox"]->getValue());
    *out<<pre(i2cComm.getLog())<<endl;
    insertI2cDescription(out, false);
}

/// Generate one text values file for the current CBC
void BoardSupervisor::Supervisor::I2cSaveToFile(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
    out->getHTTPResponseHeader().addHeader("type", "application/x-download");
    out->getHTTPResponseHeader().addHeader("content-disposition", string("attachment;filename=").append(i2cComm.getValuesFile()).append(".txt"));
    string strContent = i2cComm.getValuesAsText(pI2cBoard->getCbcInterface(), pI2cBoard->getBeBoard(), i2cComm.getValuesFile());
    std::ostringstream ostream;
    ostream<<strContent.length();
    out->getHTTPResponseHeader().addHeader("content_length",ostream.str());
    *out<<strContent;
}

///CBC hard reset button of the main page
void BoardSupervisor::Supervisor::CbcHardReset(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	setHardResetValue(RESET_HARD, in, out);
}
///CBC fast reset button of the main page 
void BoardSupervisor::Supervisor::CbcReset101(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	setHardResetValue(RESET_FAST, in, out);
}

void BoardSupervisor::Supervisor::Test(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	//*out<<DAQData::calcCrc16FromFile(cgi["testEdit"]->getValue());	
	*out<<mainBoard.launchTest(cgi["testEdit"]->getIntegerValue());	
}

void BoardSupervisor::Supervisor::LoadFromFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    cgicc::Cgicc cgi(in);
    cgicc::form_iterator iteFile = cgi.getElement("htmlValuesFile");
    if (iteFile != cgi.getElements().end()) {
    	if (acquisition.getParameterSet().loadParamValuePairsFromFile(**iteFile))
    		AcquisitionParam(in, out);
    	else
     		LOG4CPLUS_ERROR(this->getApplicationLogger(), toolbox::toString("Error while opening file: %s", (**iteFile).c_str()));
    } 
}

void BoardSupervisor::Supervisor::SaveToFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	out->getHTTPResponseHeader().addHeader("type", "application/x-download");
	out->getHTTPResponseHeader().addHeader("content-disposition", "attachment;filename=supervisorValues.txt");
	string strHtmlValues = acquisition.getParameterSet().nameAndValuePairs(); 
	std::ostringstream ostream;
	ostream<<(strHtmlValues.length());
	out->getHTTPResponseHeader().addHeader("content_length",ostream.str());
    *out<<strHtmlValues;
}

void BoardSupervisor::Supervisor::LoadSetupFromFile(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    cgicc::Cgicc cgi(in);
    cgicc::form_iterator iteFile = cgi.getElement("htmlValuesFile");
    if (iteFile != cgi.getElements().end()) {
    	if (acquisition.getConfigParameterSet().loadParamValuePairsFromFile(**iteFile) 
    			&& acquisition.getFeCbcParameterSet().loadParamValuePairsFromFile(**iteFile))
    		Default(in, out);
    	else
     		LOG4CPLUS_ERROR(this->getApplicationLogger(), toolbox::toString("Error while opening file: %s", (**iteFile).c_str()));
    } 
}
///Save setup parameters into a text file
void BoardSupervisor::Supervisor::SaveSetupToFile(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	out->getHTTPResponseHeader().addHeader("type", "application/x-download");
	out->getHTTPResponseHeader().addHeader("content-disposition", "attachment;filename=setupValues.txt");
	string strHtmlValues = acquisition.getConfigParameterSet().nameAndValuePairs()+acquisition.getFeCbcParameterSet().nameAndValuePairs(); 
	std::ostringstream ostream;
	ostream<<(strHtmlValues.length());
	out->getHTTPResponseHeader().addHeader("content_length",ostream.str());
    *out<<strHtmlValues;
}

void BoardSupervisor::Supervisor::setHardResetValue(int iVal, xgi::Input * in, xgi::Output * out ){
   try {
   	mainBoard.sendResetCommand(iVal);
        Default(in, out);
    } catch(const std::exception& e) {
        XCEPT_RAISE(xgi::exception::Exception,  gFedDebugString_+e.what());
    }
}

void BoardSupervisor::Supervisor::FpgaConfig(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	int iUpload=mainBoard.isUploadingFpga();
	if (iUpload>0){
		iAutoRefresh=2;
		createHtmlHeader(out, VIEW_TYPE_FPGA,"fpga");
	} else {
		iAutoRefresh=0;
		createHtmlHeader(out, VIEW_TYPE_FPGA);
	}
	if (mainBoard.getBeBoardInterface()){
	    if (bPlaybackMode){
		    *out<<h3("Playback mode, no firmware information.")<<endl;
		    return;
	    }
	    try{
		mainBoard.getParameterSetInfo().readHtmlFormAndValues(fileFpgaHtml.toString());
		*out<<mainBoard.getParameterSetInfo().transformHtmlFileDescription(fileFpgaHtml.toString(),"");
	    } catch(const std::exception& ex){
		*out<< "Error while filling HTML form: "<<fileFpgaHtml.toString()<<br()<<endl<<ex.what()<<br()<<endl;
	    }
	    if (iUpload==0){
		    *out<<form().set("action","rebootBoard").set("method","POST").set("name","frmList");
		    *out<<table()<<tr()<<endl;
		    *out<<td()<<"FPGA images"<<br()<<endl;
		    *out<<cgicc::select().set("multiple").set("name","list").set("size","10").set("onchange",
			mainBoard.getBeBoard()->getBoardType()=="CTA" ? "" : "document.frmUpload.imgName.value=document.frmList.list.options[document.frmList.list.selectedIndex].text");
		    mainBoard.insertFpgaImages(out);
		    *out<<cgicc::select()<<td()<<td()<<endl;
		    *out<<input().set("type","submit").set("value","List images").set("title","List available FPGA images").set("name","action")<<br()<<endl;
		    if (mainBoard.getBeBoard()->getBoardType()=="CTA")		    
			    *out<<input().set("type","submit").set("value","Delete!").set("name","action").set("title","Delete the selected FPGA image!")<<br()<<endl;

		    *out<<"Any of the following actions will require the XDAQ processes to be restarted:<br>"<<endl;
		    *out<<input().set("type","submit").set("value","Jump to image").set("name","action").set("title","Jump to the selected FPGA image")<<br()<<endl;
		    *out<<input().set("type","submit").set("value","Reboot board").set("title","Reset FLASH and trigger FPGA configuration (asserting the program_b pin)").set("name","action")<<br()<<endl;

		    *out<<td()<<tr()<<table()<<form()<<endl;
		    *out<<form().set("action", "uploadImage").set("name","frmUpload").set("method","POST");
		    *out<<table()<<tr()<<td();
		    *out<<"New image: "<<input().set("type","text").set("size","10").set("name","imgName").set("title","Name of the FPGA configuration to be uploaded")
				.set(mainBoard.getBeBoard()->getBoardType()=="CTA" ? "alt":"readonly")<<br()<<endl;
		    *out<<td()<<td();
		    *out<<"File: "<<input().set("type","text").set("size","50").set("name","mcsFile").set("title","MCS or BIT file containing an FPGA configuration")<<endl;
		    *out<<td()<<td();
		    *out<<input().set("type","submit").set("value","Upload image").set("title","Upload the following file into the named FPGA image. The other programs connected to the board have to be closed.")<<endl;    
		    *out<<td()<<tr()<<table()<<endl;
		    *out<<form()<<endl;
	    } else 
		    *out<<mainBoard.htmlFpgaProgressBar()<<endl;

	} else
		*out<<strong("Error: board not initialized!");
    //xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Supervisor::RebootBoard(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	cgicc::form_iterator iteImage = cgi.getElement("list");
	cgicc::form_iterator iteAction = cgi.getElement("action");
	if (**iteAction == "Reboot board")
		mainBoard.reboot();
	else if (**iteAction == "List images")
		mainBoard.loadFpgaImages(); 
	else if (iteImage==cgi.getElements().end())
		*out<<b("Please select an image first")<<br()<<endl;
	else if (**iteAction == "Delete!"){
		mainBoard.getBeBoardInterface()->DeleteFpgaConfig(mainBoard.getBeBoard(), **iteImage);
		mainBoard.loadFpgaImages();
	} else if (**iteAction == "Jump to image")
		mainBoard.jumpToImage(**iteImage);

	FpgaConfig(in, out);
}

void BoardSupervisor::Supervisor::UploadImage(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	cgicc::form_iterator iteFile = cgi.getElement("mcsFile");
	cgicc::form_iterator iteImage = cgi.getElement("imgName");
	if (iteFile != cgi.getElements().end())
    	try{	
    		mainBoard.uploadFpgaConfig(**iteImage, **iteFile);
		FpgaConfig(in, out);
    	} catch(const std::string& e) {
    		*out<<"Error while uploading FPGA configuration from file: "<<**iteFile<<" into image name: "<<**iteImage<<endl;
    		*out<<e<<endl;
    	}
}

void BoardSupervisor::Supervisor::setAutoRefresh(int iRefresh){
	iAutoRefresh=iRefresh;
}

void BoardSupervisor::Supervisor::WebShowStateMachine(xgi::Output * out) throw (xgi::exception::Exception) {
    try {
        std::string action = toolbox::toString("/%s/fsmTransition",getApplicationDescriptor()->getURN().c_str());

        // display FSM
        std::set<std::string> possibleInputs = fsm_.getInputs(fsm_.getCurrentState());
        std::set<std::string> allInputs = fsm_.getInputs();

        *out << "<table border cellpadding=4 cellspacing=4 bgcolor=\"#EEEEFF\">" << std::endl;
        *out << "<tr>" << std::endl;
        *out << "<th colspan=4>" << fsm_.getStateName(fsm_.getCurrentState()) << "</th>" << std::endl;
        *out << td(small(a("Refresh").set("href", ".")))//toolbox::toString("/%s",getApplicationDescriptor()->getURN().c_str()))))
        		.set("colspan", "2").set("align", "center")<<endl;
        *out << td(small(a("State diagram").set("href", "fsmDiagram"))).set("colspan", "3").set("align", "center")<<endl;
        *out << "</tr>" << std::endl;
        *out << "<tr>" << std::endl;
        std::set<std::string>::iterator i;
        for ( i = allInputs.begin(); i != allInputs.end(); i++) {
            if ( (*i) != string("x") && (*i) != string("F") && ((*i).find("Done") == std::string::npos) ) {

                *out << "<td>";
                *out << cgicc::form().set("method","get").set("action", action).set("enctype","multipart/form-data") << std::endl;

                if ( possibleInputs.find(*i) != possibleInputs.end() ) {
                    *out << cgicc::input().set("type", "submit").set("name", "transition").set("value", (*i) );
                } else {
                    *out << cgicc::input().set("type", "submit").set("name", "transition").set("value", (*i) ).set("disabled", "true");
                }

                *out << cgicc::form();
                *out << "</td>" << std::endl;
            }
        }

        *out << "</tr>" << std::endl;
        *out << "</table>" << std::endl;
//        *out << a("GFed view").set("href", "toggleView")<<endl;

    } catch(xgi::exception::Exception &e) {
        XCEPT_RETHROW(xgi::exception::Exception, "Exception caught in WebShowRun", e);
    }
}

bool BoardSupervisor::Supervisor::initialising(toolbox::task::WorkLoop* wl) {
    try {
	fileHwDescription=Board::expandEnvironmentVariables(fileHwDescription.toString());
    	std::cout << "Using description file " << fileHwDescription.toString() << std::endl;
        if ( ! boost::algorithm::to_lower_copy(fileHwDescription.toString().substr(0,7)).compare("file://"))
    		fileHwDescription=fileHwDescription.toString().substr(7);

	mainBoard.initialize(fileHwDescription.toString(), &acquisition.getParameterSet());
	acquisition.setBoard(mainBoard.getBeBoardInterface(), mainBoard.getCbcInterface(), mainBoard.getBeBoard());
	acquisition.initFeCbcParameterSet();
        if (!bPlaybackMode && fileHwDescription.toString().length()){
		mainBoard.loadStatus();
		acquisition.setFwVersion(mainBoard.getFirmwareVersion());
	}
	fileHwDescriptionI2c=Board::expandEnvironmentVariables(fileHwDescriptionI2c.toString());
    	std::cout << "Using I2C description file " << fileHwDescriptionI2c.toString() << std::endl;
        if ( ! boost::algorithm::to_lower_copy(fileHwDescriptionI2c.toString().substr(0,7)).compare("file://"))
    		fileHwDescriptionI2c=fileHwDescriptionI2c.toString().substr(7);

        if (fileHwDescriptionI2c.toString().length() && fileHwDescriptionI2c.toString().compare(fileHwDescription.toString())){
		pI2cBoard = new Board();
                pI2cBoard->initialize(fileHwDescriptionI2c.toString(), NULL);
		//acquisition.setBoard(mainBoard.getBeBoardInterface(), mainBoard.getCbcInterface(), mainBoard.getBeBoard());
		//acquisition.initFeCbcParameterSet();
	} else
		pI2cBoard=&mainBoard;

    	//std::cout << "Status loaded" << std::endl << std::endl;
	fileParamHtml=Board::expandEnvironmentVariables(fileParamHtml);
    	if ( ! boost::algorithm::to_lower_copy(fileParamHtml.toString().substr(0,7)).compare("file://"))
    		fileParamHtml=fileParamHtml.toString().substr(7);
        	
        //string strDefault = fileParamHtml.toString()+DEFAULT_VALUES_EXTENSION;
        //if (access( strDefault.c_str(), F_OK ) != -1 )
        	//acquisition.getParameterSet().loadParamValuePairsFromFile(strDefault);
        fileSetupHtml=Board::expandEnvironmentVariables(fileSetupHtml.toString());
        if ( ! boost::algorithm::to_lower_copy(fileSetupHtml.toString().substr(0,7)).compare("file://"))
    		fileSetupHtml=fileSetupHtml.toString().substr(7);
        
	fileFpgaHtml=Board::expandEnvironmentVariables(fileFpgaHtml.toString());
        if ( ! boost::algorithm::to_lower_copy(fileFpgaHtml.toString().substr(0,7)).compare("file://"))
    		fileFpgaHtml=fileFpgaHtml.toString().substr(7);
        	
	    if (!mainBoard.getI2cDirectory().empty())
	    	i2cComm.setValuesDirectory(mainBoard.getI2cDirectory());//strI2cDirectory.toString());
    } catch(std::exception& e) {
        LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    }
    streamer.initAction();
    fsm_.fireEvent("InitialiseDone",this);
    return false;
}

bool BoardSupervisor::Supervisor::configuring(toolbox::task::WorkLoop* wl) {
    if (mainBoard.getBeBoardInterface() && !bPlaybackMode){
	//acquisition.getConfigParameterSet().readAllValuesAndWriteIntoBoard(NULL, true);
	if (mainBoard.getBeBoard()->getBoardType()=="GLIB") 
		acquisition.getFeCbcParameterSet().readAllValuesAndWriteIntoBoard(NULL, true);

	acquisition.configure(false);
	mainBoard.sendResetCommand(RESET_HARD);
        mainBoard.loadStatus();//temporary: numbers of CBC and FE are saved by soft
    }
    fsm_.fireEvent("ConfigureDone",this);
    return false;
}
bool BoardSupervisor::Supervisor::enabling(toolbox::task::WorkLoop* wl) {
    acquisition.getParameterSet().setValuesFromSerializable();
    acquisition.getStreamerParameterSet().setValuesFromSerializable();
    streamer.setConditionValuesFromVector();
    streamer.startAction();
    iCurrentPage=VIEW_TYPE_ACQUISITION;
    fsm_.fireEvent("EnableDone",this);
    return false;
}
bool BoardSupervisor::Supervisor::halting(toolbox::task::WorkLoop* wl) {
    fsm_.fireEvent("HaltDone",this);
    return false;
}
bool BoardSupervisor::Supervisor::pausing(toolbox::task::WorkLoop* wl) {
    acquisition.togglePause();
    fsm_.fireEvent("PauseDone",this);
    return false;
}
bool BoardSupervisor::Supervisor::resuming(toolbox::task::WorkLoop* wl) {
    acquisition.togglePause();
    fsm_.fireEvent("ResumeDone",this);
    return false;
}
bool BoardSupervisor::Supervisor::stopping(toolbox::task::WorkLoop* wl) {
    streamer.stopAction();
    iCurrentPage=VIEW_TYPE_ACQUISITION;
    fsm_.fireEvent("StopDone",this);
    return false;
}
bool BoardSupervisor::Supervisor::destroying(toolbox::task::WorkLoop* wl) {
    fBoardMutex.lock();
    mainBoard.destroy();
    streamer.destroyAction();
    if (pI2cBoard!=&mainBoard)
	delete pI2cBoard;

    fsm_.fireEvent("DestroyDone",this);
    fBoardMutex.unlock();
    return false;
}
