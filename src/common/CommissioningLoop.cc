/* Copyright 2014 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:5/05/2014
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		CommissioningLoop.cc
   Content : 		Performs acquisition loops with varying I2C parameter values
   Used in : 		CBCDAQ
*/
#include <boost/algorithm/string/replace.hpp>
#include "toolbox/string.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "HWDescription/BeBoard.h"
#include "HWDescription/Module.h"
#include "HWDescription/Cbc.h"
#include "HWInterface/CbcInterface.h"
#include "CommissioningLoop.h"
#include "I2cCommunication.h"
#include "Board.h"
#include "Acquisition.h"
#include "FedEvent.h"

using namespace std;
using namespace cgicc;

CommissioningLoop::CommissioningLoop()
{
	arrCommLoopVisu=nullptr;
}

CommissioningLoop::~CommissioningLoop()
{
	delete arrCommLoopVisu;
}

void CommissioningLoop::setI2cDirectory(const std::string& strDir){
	strI2cDirectory=strDir;
}
/** Insert the HTML commissioning loops configuring form.
 * @param out HTML XDAQ output
 * @param pSetConfig parameter set with general Front End board configuration
 * @param pSetFeCbc Front and CBC enabling data parameter set
 */ 
void CommissioningLoop::insertHtmlForm(xgi::Output *out,Board* pBoard, ParameterSetUhal& pSetFeCbc){
	beBoard = pBoard->getBeBoard();
	ppSetFeCbc=&pSetFeCbc;
	if (beBoard==NULL){
		*out<<h3("Board not yet configured!")<<endl;
		return;
	}
	if (pBoard->getI2cDirectory().empty()){
		*out<<"No CBC_Files path in HW description XML file";
		return;
	}
	if (!pSet.containsIntValue("param0")){
		*out<<"No parameter configured "<<form().set("action", "addCommissioningParameter");
		*out<<input().set("type", "submit").set("value", "Add")<<form();
	} else {
//		pSet.clearValues();
		if (strChoices.empty())
			readI2cParameters();
			
		int iLig = 0, iSpace;
		*out<<form().set("method","POST").set("action","validCommissioningLoop")<<endl;
		*out<<table().set("border","1");
		*out<<tr()<<th("Parameter")<<th("Minimum")<<th("Maximum")<<th("Step")<<th("Array")<<th("CBC")<<tr()<<endl;
		string strName = toolbox::toString("param%d",iLig);
		while (pSet.containsIntValue(strName)){
			string strMin = toolbox::toString("min%d",iLig);
			string strMax = toolbox::toString("max%d",iLig);
			string strStep = toolbox::toString("step%d",iLig);
			string strArray = toolbox::toString("array%d",iLig);
			*out<<tr()<<td();
			for (iSpace=0; iSpace<iLig; iSpace++) 
				*out<<" - ";
				
			*out<<cgicc::select().set("name",strName);
			ostringstream ostream, oStrName;
			oStrName<<strName<<"(select)"<<strChoices;
			pSet.insertOptionsForNode(ostream, oStrName.str());
			*out<<ostream.str()<<cgicc::select()<<td()<<endl;
			*out<<td(input().set("type","text").set("value",toolbox::toString("%d",pSet.getValue(strMin))).set("size","4").set("name",strMin))<<endl;
			*out<<td(input().set("type","text").set("value",toolbox::toString("%d",pSet.getValue(strMax))).set("size","4").set("name",strMax))<<endl;
			*out<<td(input().set("type","text").set("value",toolbox::toString("%d",pSet.getValue(strStep))).set("size","4").set("name",strStep))<<endl;
			*out<<td(input().set("type","text").set("value",pSet.getStrValue(strArray)).set("size","8").set("name",strArray))<<endl;
			*out<<td();
			insertCbcTable(out, iLig);
			*out<<td();
			*out<<tr()<<endl;
			iLig++;
			strName = toolbox::toString("param%d",iLig);
		}
		*out<<table()<<endl;
		*out<<input().set("type", "submit").set("value", "OK")<<form()<<br()<<endl;
		*out<<table()<<tr()<<td()<<form().set("action","addCommissioningParameter");
		*out<<input().set("type", "submit").set("value", "Add one line")<<form()<<td()<<tr()<<endl;
		*out<<tr()<<td()<<form().set("action","removeLastCommissioningParameter");
		*out<<input().set("type", "submit").set("value", "Remove last line")<<form()<<td()<<tr()<<table()<<endl;
	}
	//Save into a file or load from 
	*out<<"<br/><table><tr><td>";
	*out<<"<form action='saveCommissioningLoop'><input type='submit' value='Save to file'/></form> "<<endl;
	*out<<"</td><td>";
	*out<<"<form action='loadCommissioningLoop'><input type='submit' value='Load from file'/>";
	*out<<"<input type='text' size='40' name='htmlValuesFile'/></form>"<<endl;
	*out<<"</td></tr></table>"<<endl;
}
///Insert one CBC sub table into the table of commissioning loops
void CommissioningLoop::insertCbcTable(xgi::Output *out, int iLig){
	int iFE, nbFE=beBoard->getNFe();
	int iCBC, nbCBC=beBoard->fModuleVector[0]->getNCbc();
	*out<<"<table border='1'><tr><td></td>";
	for (iCBC=0; iCBC<nbCBC; iCBC++)
		*out<<th(toolbox::toString("%C%d", (char)('A'+iCBC%2), iCBC/2));
		
	*out<<"</tr>"<<endl;
	for (iFE=0; iFE<nbFE; iFE++){
//		if (nbFE==1) iFE=1;

		*out<<"<tr>"<<th(toolbox::toString("FE %d",iFE));
		for (iCBC=0; iCBC<nbCBC; iCBC++){
			*out<<"<td align='center'>";
			if (ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_ENABLED, iFE))==1
					&& ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED, iFE, iCBC))==1){
				
				*out<<pSet.htmlDescription(toolbox::toString("param%dfe%dcbc%d",iLig ,iFE, iCBC),CHECKBOX,"")<<"</td>"<<endl;
			} else 
				*out<<"-</td>"<<endl;
		}
		*out<<"</tr>";
	}
	*out<<"</table>"<<endl;
}
/** Generate the content of an HTML combo box component with all parameter names indexed by their page and address. It is assumed that all CBCs have the same parameters set.*/
void CommissioningLoop::readI2cParameters(){
	std::ostringstream ostream;
	strChoices.clear();
 	mapI2cNames.clear();
	Ph2_HwDescription::Cbc* pCbc = beBoard->fModuleVector[0]->fCbcVector[0];
	string strName;
	for (Ph2_HwDescription::CbcRegMap::iterator cIt = pCbc->getRegMap().begin(); cIt!=pCbc->getRegMap().end(); cIt++){
		strName = cIt->first;
		uint16_t uKey =cIt->second.fPage*255 + cIt->second.fAddress;
		mapI2cNames[uKey ] = strName;
		boost::replace_all(strName, "&", "&amp;");
		boost::replace_all(strName, "<", "&lt;");
		boost::replace_all(strName, ">", "&gt;");
		ostream<<toolbox::toString("%d: %s,",uKey ,strName.c_str());
	}
//Read a CBC I2C values file to generate an HTML description of a combo box content with all parameter names 
	/* I2cCommunication i2cComm("","","",0);
	uint32_t uPage, uAddr, uDef, uRead, uWrite, uLine=0;
	std::ifstream filValues(toolbox::toString("%s/FE0CBC0.txt",strI2cDirectory.c_str()).c_str());
	string strLine, strName;
	while (filValues.good()){//Read parameter names and page numbers and addresses
		getline(filValues, strLine);
		uLine++;uDef=0;uRead=0;uWrite=0;
		if (strLine[0]!='#' && strLine[0]!='*' && strLine.length()>0){
			if (i2cComm.valuesFromLine(strLine, false, strName, &uPage, &uAddr, &uDef, &uWrite)){
				boost::replace_all(strName, "&", "&amp;");
				boost::replace_all(strName, "<", "&lt;");
				boost::replace_all(strName, ">", "&gt;");
				ostream<<toolbox::toString("%d: %s,",uPage*255+uAddr,strName.c_str());
			}
		}
	}//while
	filValues.close();*/
	strChoices=ostream.str();
}
/// Add one commissioning loop
void CommissioningLoop::addOneParameter(){
	int iLig=0;
	while (pSet.containsIntValue(toolbox::toString("param%d",iLig))){
		iLig++;
	}
	pSet.setValue(toolbox::toString("param%d",iLig), 0);
	pSet.setValue(toolbox::toString("step%d",iLig) , 1);
	int iFE, nbFE=beBoard->getNFe();
	int iCBC, nbCBC=beBoard->fModuleVector[0]->getNCbc();
	for (iFE=0; iFE<nbFE; iFE++){//Validated CBCs are checked
//		if (nbFE==1) iFE=1;

		for (iCBC=0; iCBC<nbCBC; iCBC++){
//			if (ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_ENABLED, iFE))==1 && ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED, iFE, iCBC))==1)//temporary
				pSet.setValue(toolbox::toString("param%dfe%dcbc%d",iLig ,iFE, iCBC),1);
		}
	}
}
///Remove the last configured commissioning loop
void CommissioningLoop::removeLastParameter(){
	int iLig=0;
	while (pSet.containsIntValue(toolbox::toString("param%d",iLig))){
		iLig++;
	}
	if (iLig>0)
		pSet.removeIntValue(toolbox::toString("param%d",iLig-1));
}
///Compute the number of iterations configured (the commissioning loops are nested)
uint32_t CommissioningLoop::calcNbIterations(){
	nbIterations=0;
	int iLig=0;
	while (pSet.containsIntValue(toolbox::toString("param%d",iLig))){
		if (nbIterations==0)
			nbIterations=1;
			
		nbIterations*=(pSet.getValue(toolbox::toString("max%d",iLig)) - pSet.getValue(toolbox::toString("min%d",iLig)))
				/ pSet.getValue(toolbox::toString("step%d", iLig)) + 1;
		iLig++;
	}
	return nbIterations;
}
///Retrieve commissioning parameter values from HTML components	
void CommissioningLoop::setValuesFromGui(cgicc::Cgicc& cgi){
	int iLig=0;
	while (pSet.containsIntValue(toolbox::toString("param%d",iLig))){
		pSet.setFromGui(cgi, toolbox::toString("param%d",iLig), COMBOBOX);
		pSet.setFromGui(cgi, toolbox::toString("min%d",iLig), INTEGER);
		pSet.setFromGui(cgi, toolbox::toString("max%d",iLig), INTEGER);
		pSet.setFromGui(cgi, toolbox::toString("step%d",iLig), INTEGER);
		pSet.setFromGui(cgi, toolbox::toString("array%d",iLig), TEXTFIELD);
		if (pSet.getValue(toolbox::toString("step%d",iLig))==0)
			pSet.setValue(toolbox::toString("step%d",iLig), 1);//step value cannot be 0
			
		if (pSet.getValue(toolbox::toString("min%d",iLig))>pSet.getValue(toolbox::toString("max%d",iLig))){//swap min and max
			uint32_t uMin = pSet.getValue(toolbox::toString("min%d",iLig));
			pSet.setValue(toolbox::toString("min%d",iLig), pSet.getValue(toolbox::toString("max%d",iLig)));
			pSet.setValue(toolbox::toString("max%d",iLig), uMin);
		}
		int iFE, nbFE=beBoard->getNFe();
		int iCBC, nbCBC=beBoard->fModuleVector[0]->getNCbc();
		for (iFE=0; iFE<nbFE; iFE++){//Validated CBCs are checked
//			if (nbFE==1) iFE=1;
	
			for (iCBC=0; iCBC<nbCBC; iCBC++){
				if (ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_ENABLED, iFE))==1
						&& ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED, iFE, iCBC))==1)				
					pSet.setFromGui(cgi, toolbox::toString("param%dfe%dcbc%d",iLig ,iFE, iCBC),CHECKBOX);
			}
		}
		iLig++;
	}
}
///Initialization of commissioning loop acquisitions. Reset the loop number.
void CommissioningLoop::initAcq(){
	numLoop=0;
	delete arrCommLoopVisu;
	arrCommLoopVisu=nullptr;
	vecCommLoopString.clear();
	vecNbEvent.clear();
	lastEvtCounter=0;
	calcNbIterations();
}
///Go to next commissioning loop step and write I2C values into CBC
void CommissioningLoop::nextAcq(Ph2_HwInterface::CbcInterface *cbcInterface){
	int nbLig=0, iLig;
	uint32_t num = numLoop, nb, uVal;
	while (pSet.containsIntValue(toolbox::toString("param%d",nbLig))){
		nbLig++;
	}
	nbLig--;
	vector<uint32_t> vecReq;
	strLog="";
	strCurrentValues="";
	for (iLig=nbLig; iLig>=0; iLig--){
		nb=(pSet.getValue(toolbox::toString("max%d",iLig))- pSet.getValue(toolbox::toString("min%d",iLig)))
			/ pSet.getValue(toolbox::toString("step%d",iLig))+1;
			
		string strParamArray=toolbox::toString("array%d",iLig);
		if (pSet.containsStrValue(strParamArray) && pSet.getStrValue(strParamArray).length()>0){
			istringstream iss(pSet.getStrValue(strParamArray));
			string strVal;
			for (uint32_t idx=0; idx <= num%nb; idx++)
				iss >> strVal;
			
			try{
			  uVal=stoul(strVal);
			} catch (const std::invalid_argument& ia) {
	  		  strLog.append(toolbox::toString("ERROR : Cannot convert %s into unsigned integer\n", strVal.c_str()));
			  uVal=0;
			}

		} else
			uVal=pSet.getValue(toolbox::toString("min%d",iLig))+pSet.getValue(toolbox::toString("step%d",iLig)) * (num % nb);

		strCurrentValues.append( addI2cRequestsForParam(cbcInterface, iLig, uVal) );
			
		num /= nb;
	}
	
	numLoop++;
}

std::string CommissioningLoop::addI2cRequestsForParam(Ph2_HwInterface::CbcInterface *cbcInterface, int iLig, uint32_t uWrite){
	uint32_t uAddr  = pSet.getValue(toolbox::toString("param%d",iLig));
	uint32_t uFE;//, nbFE=beBoard->getNFe();
	uint32_t uCBC;//, nbCBC=beBoard->fModuleVector[0]->getNCbc();
	std::string strRet = toolbox::toString("%s = 0x%02X  ", mapI2cNames[uAddr].c_str(), uWrite);
	/*for (uFE=0; uFE<nbFE; uFE++){//
//		if (nbFE==1) uFE=1;

		if (ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_ENABLED, uFE))==1)
			for (uCBC=0; uCBC<nbCBC; uCBC++)
				if (ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED, uFE, uCBC))==1				
						&& pSet.getValue(toolbox::toString("param%dfe%dcbc%d",iLig ,uFE, uCBC))==1)
					I2cCommunication::blockAdd(vecReq, uFE, uCBC, uAddr/255, uAddr%255, uWrite);
	}$*/
	for ( std::vector<Module*>::iterator cModuleIt = beBoard->fModuleVector.begin(); cModuleIt != beBoard->fModuleVector.end(); cModuleIt++ ){
		uFE = (*cModuleIt)->getModuleId();
		if (ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_ENABLED, uFE))==1)
		for ( std::vector<Cbc*>::iterator cCbcIt = (*cModuleIt)->fCbcVector.begin(); cCbcIt != (*cModuleIt)->fCbcVector.end(); cCbcIt++ ){
			uCBC = (*cCbcIt)->getCbcId();
			if (ppSetFeCbc->getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED, uFE, uCBC))==1				
					&& pSet.getValue(toolbox::toString("param%dfe%dcbc%d",iLig ,uFE, uCBC))==1){
				cbcInterface->WriteCbcReg( *cCbcIt, mapI2cNames[uAddr], uWrite, true );
				strLog.append(toolbox::toString("CBC#%d: %s = 0x%02X, ",uCBC, mapI2cNames[uAddr].c_str(), uWrite));
			}
		}
	}			
	return strRet;
}

void CommissioningLoop::storeVisuData(uint32_t* arrData, uint32_t event_counter){
	uint32_t nbBin=NB_STRIPS_CBC2;
	if (arrCommLoopVisu==nullptr){
		arrCommLoopVisu = new uint32_t[nbBin*nbIterations]; 
		memset(arrCommLoopVisu, 0, sizeof(uint32_t)*nbBin*nbIterations);
	}
	memcpy(&arrCommLoopVisu[(numLoop-1) * nbBin], arrData, sizeof(uint32_t)*nbBin);
	vecCommLoopString.push_back(getCurrentValuesString());
	vecNbEvent.push_back(event_counter - lastEvtCounter);
	lastEvtCounter = event_counter;
}

std::string CommissioningLoop::getDataVisuCommLoopString(uint32_t iCom){
	if (iCom<vecCommLoopString.size())
		return vecCommLoopString.at(iCom);
	else 
		return "";
}

uint32_t* CommissioningLoop::getDataVisuCommLoop(uint32_t iCom){
	uint32_t nbBin=NB_STRIPS_CBC2;
	return &arrCommLoopVisu[iCom*nbBin];
}
	
uint32_t CommissioningLoop::getCommLoopNbEvent(uint32_t iCom){
	if (iCom<vecNbEvent.size())
		return vecNbEvent.at(iCom);
	else 
		return 0;
}

uint32_t* CommissioningLoop::createSumArray(){
	uint32_t uCom, uStrip, uSum, nbBin=NB_STRIPS_CBC2, uNbEvt;
	uint32_t* puRet=new uint32_t[nbBin];

	memset(puRet, 0, sizeof(uint32_t) * nbBin);
	for (uCom=0; uCom<nbIterations; uCom++){
		uNbEvt=getCommLoopNbEvent(uCom);
		if (uNbEvt>0){
			uSum=0;
			for(uStrip=0; uStrip<nbBin; uStrip++)	
				uSum+=arrCommLoopVisu[nbBin*uCom + uStrip];

			puRet[uCom]=1000 * uSum / uNbEvt / nbBin;
		} else 
			puRet[uCom]=0;
	}		
	return puRet;
}
