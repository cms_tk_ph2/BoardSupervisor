#  Import the PyChips code - PYTHONPATH must be set to the PyChips installation src folder!
from PyChipsUser import *
from time import sleep
from struct import *
import sys
from time import *

# Read in an address table by creating an AddressTable object (Note the forward slashes, not backslashes!)
glibAddrTable = AddressTable("glib_ttc_fmc_AddrTable.dat")


# Create a ChipsBus bus to talk to your board.
# These require an address table object, an IP address and a port number
# f = open('./ipaddr.dat', 'r')
# ipaddr = f.readline()
# f.close()
ipaddr = "192.168.0.111"
glib = ChipsBusUdp(glibAddrTable, ipaddr, 50001)
print
print "--=======================================--"
print "  Opening GLIB with IP", ipaddr
print "--=======================================--"

#display glib_id
print "\nboard_id is:", hex(glib.read("board_id"))

#id�es

#comme sur glib_i2c_eeprom_eui
# i2c_en    = 1
# i2c_sel   = 0
# i2c_presc = 500
# i2c_settings_value = i2c_en*(2**11) + i2c_sel*(2**10) + i2c_presc
# glib.write("i2c_settings", i2c_settings_value)


#param i2c offset / wb_reg
reg_i2c_settings_offset = 24
reg_i2c_command_offset  = 25
reg_i2c_reply_offset    = 26

# reg @ of cbc
# FrontEndControl         0x00
# TriggerLatency          0x01
# HitDetectSLVS           0x02
# Ipre1                   0x03
# Ipre2                   0x04
# Ipsf                    0x05
# Ipa                     0x06
# Ipaos                   0x07
# Ifpa                    0x08
# Icomp                   0x09
# Vpc                     0x0a
# Vplus                   0x0b
# VCth                    0x0c
# Channel0                0x80
#...
# Channel127              0xff


# param i2c settings 
i2c_ctrl_disable = 0x00000000 #settings to disable
#i2c_ctrl_enable  = 0x000009F4 # equal as follow
i2c_en    = 1
i2c_sel   = 0 #?
i2c_presc = 500 #0 to 1023 --> 62.5M/i2c_presc => here : 125kHz
i2c_ctrl_enable = i2c_en*(2**11) + i2c_sel*(2**10) + i2c_presc


# param i2c command <=> strobe valid / write mode / 8b / RalMode
dummy = 0x00	
#command
strobe = 1 
m16b = 0 #8b 
mem = 1 #ral mode ???
slave_addr = 0x5b #cbc_i2c_slave_addr
wr = 1 #0:read / 1:write
#wrdata = dummy



#LISTES regAddr + regName
###=>LISTE1 : vecteur
##regAddr = [0x00		        ,0x01			,0x02			,0x03		,0x04		,0x05		,0x06		,0x07		,0x08		,0x09		,0x0a		,0x0b		,0x0c]
##regName = ["(FrontEndControl)"	,"(TriggerLatency)"	,"(HitDetectSLVS)"	,"(Ipre1)     "	,"(Ipre2)     "	,"(Ipsf)      "	,"(Ipa)      "	,"(Ipaos)     "	,"(Ifpa)       "	,"(Icomp)     "	,"(Vpc)       "	,"(Vplus)     "	,"(VCth)      "]
###wrdata_table   = [0x00					,0x00				,0x00				,0x00,		,0x00		,0x00		,0x00		,0x00		,0x00		,0x00		,0x00		,0x00		,0x00]    

#=>LISTE2 : lecture d'un fichier
i2cAddrTableFile = "CBCv1_i2cSlaveAddrTable.txt"


#conna�tre le nombre exact de registres
file = open(i2cAddrTableFile, 'r')
line = file.readline() # Get the first line
lineNum = 0
while len(line) != 0:  # i.e. not the end of the file
    words = line.split()   # Split up the line into words by its whitespace
    if len(words) != 0:  # A blank line (or a line with just whitespace).
        if line[0] != '*':  # Not a commented line
            if len(words) < 4:
                raise ChipsException("Line " + str(lineNum) + " of file '" + addressTableFile + 
                                "' does not conform to file format expectations!")       
            lineNum += 1
    line = file.readline()  # Get the next line and start again.
file.close()
#print lineNum

#init vecteurs
regName = range(lineNum)
regAddr = range(lineNum)
defData = range(lineNum)
wrData  = range(lineNum)

#lecture du fichier
file = open(i2cAddrTableFile, 'r')
line = file.readline() # Get the first line
lineNum = 1
index = 0
regNameSize = 15
regNameAdd = '_'

while len(line) != 0:  # i.e. not the end of the file
    words = line.split()   # Split up the line into words by its whitespace
    if len(words) != 0:  # A blank line (or a line with just whitespace).
        if line[0] != '*':  # Not a commented line
            if len(words) < 4:
                raise ChipsException("Line " + str(lineNum) + " of file '" + addressTableFile + 
                                "' does not conform to file format expectations!")
            regName[index] = words[0] + regNameAdd * (regNameSize - len(words[0]))
            regAddr[index] = int(words[1], 16)
            defData[index] = int(words[2], 16)
            wrData[index]  = int(words[3], 16)
            index=index+1   
    line = file.readline()  # Get the next line and start again.
    lineNum += 1
file.close()


###display all items of i2cAddrTableFile
###
##print
##print "-> displaying i2cAddrTableFile"
##sleep(0.001)
##print
##for j in range (len(regName)):
##    print "regName=",regName[j],"\tregAddr=",hex(regAddr[j]),"\tdefData=",hex(defData[j])



# Config/Valid i2c controller
print
print "-> enabling cbc i2c controller  "
glib.write("user_wb_cbc_fmc_regs", i2c_ctrl_enable, reg_i2c_settings_offset)
sleep(0.001)
print



# writting all registers of CBC via i2c transaction
print
print "-> writting all registers of CBC"
print


#for j in range (0,1):#len(regAddr)):
for j in range (len(regAddr)):        
	reg_addr = regAddr[j]
	wrdata   = wrData[j]
	
	cbc_fmc_comm = strobe*(2**31) + m16b*(2**25) + mem*(2**24) + wr*(2**23) + slave_addr *(2**16) + reg_addr *(2**8) + wrdata
	glib.write("user_wb_cbc_fmc_regs", cbc_fmc_comm, reg_i2c_command_offset)
	#sleep(0.1)
	reply = glib.read("user_wb_cbc_fmc_regs", reg_i2c_reply_offset)
	#sleep(0.1)
	
	rddata = reply & 0xff
	if (reply < 0x10000000): status = "done"
	else: status = "fail"
	print "-> slave@ =",uInt8HexStr(slave_addr),"reg@ =",uInt8HexStr(reg_addr),"<=>",regName[j],"status =",status
##	print "-> slave@ =",uInt8HexStr(slave_addr),"reg@ =",uInt8HexStr(reg_addr),"<=>",regName[j],"data = ",uInt8HexStr(rddata),"status =",status
##        print "-> slave@ =",hex(slave_addr),"reg@ =",hex(reg_addr),regName[j],"data =",uInt8HexStr(rddata),"status =",status

#######################################
# reply(31)		<= error_rdack4;
# reply(30)		<= error_rdack3;
# reply(29)		<= error_rdack2;
# reply(28)		<= error_rdack1;
# reply(27)		<= error;
# reply(26)		<= done;
# reply(25)		<= extm;
# reply(24)		<= ral;
#######################################





print "-> disabling cbc i2c controller "
glib.write("user_wb_cbc_fmc_regs", i2c_ctrl_disable, reg_i2c_settings_offset)
print

print
print "-> done"
print
print "--=======================================--"
print 
print




