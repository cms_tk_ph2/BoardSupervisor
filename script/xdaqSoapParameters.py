#!/usr/bin/python

import sys
import xdglib
try:
   import elementtree.ElementTree as ET
   import sqlite as SQLITE
except:
   #print "SLC6"
   import xml.etree.ElementTree as ET
   import sqlite3 as SQLITE

if len(sys.argv)<2: 
    print "Display the parameters list of a XDAQ application\nUsage:",sys.argv[0],"<XDAQ XML configuration file> [Parameter name] [Value]"
    sys.exit()


tree=ET.parse(sys.argv[1])
for node in tree.getiterator():
  #print "tag:",node.tag
  ff=node.tag.split('}')
  if (len(ff)<2): continue;
  prefix=ff[0][1:]
  name=ff[1]
  if (name == "Context"):
    for x in node.items():
      if (x[0] == "url"):
        currentContext=x[1]
  elif (name == "Application"):
    for x in node.items():
      if x[0] == "class":
        currentApp=x[1]
      elif x[0] == "instance":
        currentInstance=x[1]
  elif name== "properties":
    #print node.items()
    #print prefix
	app=xdglib.XdaqApplication(currentContext,prefix,currentApp,currentInstance)
	app.QueryParameters()
	if len(sys.argv)>3:
		app.setParameter(sys.argv[2], sys.argv[3])
		app.updateParameters()

	app.Dump()

