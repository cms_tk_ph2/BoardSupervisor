#!python

import sys

if len(sys.argv)<2:
  print "Conversion of PyChips address table to uHal XML address table"
  print "Usage: pyChipsDat2uhalXml.py <input file>"
  print "       pyChips descriptions starting with 'top' are interpreted as the beginning of a parent node."
  print "       Descriptions starting with 'size=' and a number in Kword are interpreted as the size of a block node."
  print "       An empty line or a comment line (starting with '*') will end the parent node."
  sys.exit()

'''Read in an address table from the specified file'''

file = open(sys.argv[1], 'r')
line = file.readline() # Get the first line
lineNum = 1
nodeNum = 1
base=-1

print '<?xml version="1.0" encoding="ISO-8859-1"?>'
print '<?xml-stylesheet type="text/xsl" href="param_table.xsl"?>'
print
print '<node id="root">'

while len(line) != 0:  # i.e. not the end of the file
    words = line.split()   # Split up the line into words by its whitespace
    if base>=0 and (len(words)==0 or line[0]=='*'):
            base=-1
            print "  </node>"

    if len(words) != 0:  # A blank line (or a line with just whitespace).
        if line[0] == '*':  # commented line
            print '<!-- '+line[1:-1].replace("--","- ")+' -->'
        else:
            if len(words) < 5:
                raise Exception("Line " + str(lineNum) + " of file '" + sys.argv[1] + 
                                     "' does not conform to file format expectations!")
            try:
                regName = words[0]
                regAddr = int(words[1],16)
                regMask = words[2]
                regRead = words[3]
                regWrite= words[4]
                if len(words)>5:
                        regDesc = words[5]
                else:
                        regDesc = ""
        

            except Exception, err:
                raise Exception("Line " + str(lineNum) + " of file '" + sys.argv[1] + 
                                     "' does not conform to file format expectations! (Detail: " + str(err))
            if regMask == '0': raise Exception("Register mask on line " + str(lineNum) +
                                                  " of file '" + sys.argv[1] + 
                                                  "' is zero! This is not allowed!")
            if regRead != '0' and regRead != '1':raise Exception("Read flag on line " +
                                                                  str(lineNum) + " of file '" +
                                                                  sys.argv[1] + "' is not one or zero!")
            if regWrite != '0' and regWrite != '1': raise Exception("Write flag on line " +
                                                                     str(lineNum) + " of file '" +
                                                                     sys.argv[1] + "' is not one or zero!")
            #if base>=0 and regAddr>base:
                  #print "  </node>"
                  #base=0

            if regDesc.startswith("top"):
                if base>=0:
                        print "  </node>"

                base=regAddr
                out='  <node id="'+regName+'"	address="'+hex(regAddr)+'"	permission="'
            else:
                if base>=0:
                        regAddr -= base

                if regDesc.startswith("size="):
                    size=int(regDesc[5:].split()[0])
                    regDesc=""
                    out='    <node id="'+regName+'"	address="'+hex(regAddr)+'" mode="block"	size="'+str(size*1024)+'"	permission="'
                else:
                    out='    <node id="'+regName+'"	address="'+hex(regAddr)+'"	mask="0x'+regMask+'"	permission="'
                
            if regRead=='1':
                out+='r'

            if regWrite=='1':
                out+='w'
                
            if len(regDesc)>0 and not regDesc.startswith("top"):
                out+='" description="'+regDesc;

            if regDesc.startswith("top"):
                out+='">'
            else:
                out+='"/>'

            print out
            nodeNum+=1

    line = file.readline()  # Get the next line and start again.
    lineNum += 1

#print "  </node>"
print "</node>"
print
