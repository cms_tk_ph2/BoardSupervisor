#!/usr/bin/python
# Performance test for IPBus latency. Usage : perfTest <board name> <repetition number>

import timeit
import sys

board="localhost"
if len(sys.argv)>1:
  board=sys.argv[1]

nb=10000
if len(sys.argv)>2:
  nb=int(sys.argv[2])

output="Size (bytes);Banwidth (Mbits/s)\n"
for size in range(200, 20001, 200):
        t=timeit.Timer(setup='import uhal;manager = uhal.ConnectionManager("file:///opt/testing/trackerDAQ-3.2/CBCDAQ/BoardSupervisor/xml/connections.xml");hw = manager.getDevice("'+board+'")', 
	stmt='hw.getNode("sram1").readBlock('+str(size)+');hw.dispatch()')
        duration=t.timeit(number=nb)
        output+=str(size*4)+";"+str(size*32*nb/duration/1e6)+";"+str(duration*1000)+" ms\n"

print output

