#!/usr/bin/python

##===================================================================================================##
##==================================== Script Information ===========================================##
##===================================================================================================##
##																								 
## Company:  				CERN (PH-ESE-BE)													 
## Engineer: 				Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros@ieee.org)
## 																						 					
## Create Date:				13/03/2013													 
## Script Name:				glib_flash_user_prom_rw												 
## Python Version:			2.7.3																			
##																											
## Revision:				1.0 																				
##																													
## Additional Comments: 																							
## * Read and Write User PROM(MCS) file from/to user FLASH region (400000h-77FFFFh) using uHAL functions.
##																													
##===================================================================================================##
##===================================================================================================##

## Python modules:
import sys
import os
import binascii
import array
from time import *
import uhal

##===================================================================================================##
##==================================== Module Information ===========================================##
##===================================================================================================##
#######################################################################################################
## Constant declarations
#######################################################################################################

## Register bit masks:
regMask = []
for i in range(16):
	regMask.append(0x0000 + (1*(2**i)))

## FLASH and MCS constants:
num_param_blocks_in_param_bank = 4
num_total_blocks = 131
higher_block = 130
ela_per_block = 2
std_block_size = 64*(2**10) # Kword
max_write_buffer = 32 # 16 bit words
statusReg_width	= 8 # bits

## Golden PROM file constants:
higher_golden_block = 130
lower_golden_block  =  67 

## User PROM file constants:
higher_user_block = 66
lower_user_block  = 11

#######################################################################################################
## FLASH commands
#######################################################################################################

## (Xilinx DS617 Page 12):
blockLockConfirm_comm		= 0x0001
setConfigRegConfirm_comm    = 0x0003
altProgSetup_comm			= 0x0010
blockEraseSetup_comm		= 0x0020
blockLockDownConfirm_comm	= 0x002F
programSetup_comm			= 0x0040
clearStatusReg_comm			= 0x0050
blockLockSetup_comm			= 0x0060
blockUnlockSetup_comm		= 0x0060
blockLockDownSetup_comm		= 0x0060
setConfigRegSetup_comm		= 0x0060
readStatusReg_comm        	= 0x0070
buffEnhFactProgSet_comm		= 0x0080
readElectSignarure_comm		= 0x0090
readCfiQuery_comm			= 0x0098
progEraseSupend_comm		= 0x00B0
blankCheckSetup_comm		= 0x00BC
protectRegProgram_comm		= 0x00C0
blankCheckConfirm_comm		= 0x00CB
progEraseresume_comm		= 0x00D0
blockEraseConfirm_comm		= 0x00D0
blockUnlockConfirm_comm		= 0x00D0
bufferProgConfirm_comm		= 0x00D0
buffEnhFactProgConfirm_comm	= 0x00D0
bufferProgram_comm			= 0x00E8
readArray_comm				= 0x00FF

#######################################################################################################
## Function definitions
#######################################################################################################

def confAsyncRead(glib):	
	'''Sets the read mode as asynchronous.'''

	## Load commands in buffer:	
	comm_buffer = []	
	comm_buffer.append([setConfigRegSetup_comm, setConfigRegConfirm_comm])	
	comm_buffer.append([readElectSignarure_comm])
	
	## Process:	
	for i in range(5): # Five attempts
		##(Note!!! Configuration data for asynchronous read is provided in the address)
		glib.getClient().writeBlock(glib.getNode("flash_async_read_cmd").getAddress(),comm_buffer[0],uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch() # Configuration Register data is in the address	
		glib.getClient().writeBlock(glib.getNode("flash_block").getAddress()+0x5,comm_buffer[1], uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch()  # Configuration Register (Bank Address + 005)
		## Read to check whether CR15=1 or not)(Conf_Reg = BDDF):
		configReg = glib.getClient().read(glib.getNode("flash_block").getAddress() + 0x5);glib.dispatch()
		if configReg == 0xBDDF:
			return "-> Asynchronous read configuration done"			
	return "-> Error!!! It was not possible to set the read mode as asynchronous."
	
def blockLockOrUnlock(glib, block, operation): # 'block' is a string
	'''Locks or unlocks a block of the flash (Xilinx DS617(v3.0.1) page 75, figure 43).'''
		
	## Operation detection:
	if operation == "l":	
		comm_buffer = [blockLockSetup_comm, blockLockConfirm_comm, readElectSignarure_comm]	
		lock_status_expected = 0x0001
		lock_op = "LOCK"
	elif operation == "u":
		comm_buffer = [blockUnlockSetup_comm, blockUnlockConfirm_comm, readElectSignarure_comm]	
		lock_status_expected = 0x0000
		lock_op = "UNLOCK"	
	else:	
		return "-> Error!!! Check the value provided to the argument \"operation\"."
	
	## Process:	
	for i in range(5): # Five attempts
		glib.getClient().writeBlock(block, comm_buffer,uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch()
		## Electronic signature Codes (Xilinx DS617(v3.0.1) page 75, figure 43)
		## * Block protection (Block address + 0x2): 		
		##   - Locked:                                0x0001
		##   - Unlocked:                   			  0x0000
		##   - Note!!! Lock-Down is not suppported by this funcion
		lock_status = glib.getClient().read(block + 0x2)  ;glib.dispatch()		 
		if lock_status == lock_status_expected: 	 
			return "-> Lock state modification of %s to %s done." % (block, lock_op)		
	return "-> Error!!! It was not possible to %s %s." % (lock_op, block)
	
def blockErase(glib, block): # 'block' is a string
	'''Erases a block of the flash (Xilinx DS617(v3.0.1) page 73, figure 41).'''
	
	## Block Erase commands (2 cycles):
	comm_buffer = [blockEraseSetup_comm, blockEraseConfirm_comm]
	
	## Status Register bits declaration and initialization:
	sr = []
	for i in range(statusReg_width):
		sr.append(0)	
	
	## Process:
	glib.getClient().writeBlock(block, comm_buffer,uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch()
	while sr[7] != 1:		
		statusReg = glib.getClient().read(block);glib.dispatch()
		for i in range(statusReg_width):			
			sr[i] = (statusReg & regMask[i]) >> i	
	if sr[3] == 1 or sr[4] == 1 or sr[5] == 1 or sr[1] == 1:
		error_array = []
		if sr[3] == 1:
			error_array.append("-> Error!!! VPP Invalid.")
		if sr[4] == 1:		
			error_array.append("-> Error!!! Command sequence error.")
		if sr[5] == 1:		
			error_array.append("-> Error!!! Erase Error.")		
		if sr[1] == 1:
			error_array.append("-> Error!!! Erase to Protected Block.")
		writeBuffer = [clearStatusReg_comm]
		glib.getClient().writeBlock(block, writeBuffer,uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch()
		return error_array		
	else:
		return "-> Erase of %s done." % block

def bufferProgram(glib, block, base_offset, data, words):
	'''Writes up to 32 words to the flash (Xilinx DS617(v3.0.1) page 71, figure 39).'''
	
	## Write commands and buffer size etup:
	comm_buffer = [[bufferProgram_comm], [bufferProgConfirm_comm]]
	buffer_size = [words-1]
	
	## Status Register bits declaration and initialization:
	sr = []
	for i in range(statusReg_width):
		sr.append(0)	
	
	## Process:	
	glib.getClient().writeBlock(block + base_offset, comm_buffer[0],uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch()	
        statusReg= glib.getClient().read(block);glib.dispatch()
	while statusReg&0xFFFF != 0x0080:		
                statusReg= glib.getClient().read(block);glib.dispatch()

	glib.getClient().writeBlock(block+base_offset, buffer_size,uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch()		
	glib.getClient().writeBlock(block+base_offset, data);glib.dispatch()			
	glib.getClient().writeBlock(block+base_offset, comm_buffer[1]);glib.dispatch()
	while sr[7] != 1:				
		statusReg = glib.getClient().read(block);glib.dispatch()
		for i in range(statusReg_width):			
			sr[i] = (statusReg & regMask[i]) >> i
	if sr[3] == 1 or sr[4] == 1 or sr[1] == 1:
		error_array = []
		if sr[3] == 1:
			error_array.append("-> Error!!! VPP Invalid.")
		if sr[4] == 1:		
			error_array.append("-> Error!!! Program error.")
		if sr[1] == 1:
			error_array.append("-> Error!!! Program to Protected Block.")
		writeBuffer = [clearStatusReg_comm]
		glib.getClient().writeBlock(block, writeBuffer,uhal.BlockReadWriteMode.NON_INCREMENTAL);glib.dispatch()
		return error_array			
	else:
		return "-> Buffer program on %s at base addess %s done." % (block, hex(base_offset))
	
##===================================================================================================##
##===================================================================================================##
##======================================== Script Body ==============================================## 
##===================================================================================================##
##===================================================================================================##

args = sys.argv
arg_length = len(args)

if arg_length == 2:	

	#######################################################################################################
	## PYCHIPS
	#######################################################################################################

	#######################################################################################################
	### Uncomment one of the following two lines to turn on verbose or very-verbose debug modes.        ###
	### These debug modes allow you to see the packets being sent and received.                         ###
	#######################################################################################################

	##chipsLog.setLevel(logging.DEBUG)    # Verbose logging (see packets being sent and received)

        glib=uhal.getDevice("board","ipbusudp-2.0://192.168.000.175:50001","file:///opt/testing/trackerDAQ-3.2/CBCDAQ/BoardSupervisor/xml/address_table.xml")

	#######################################################################################################
	## Main
	#######################################################################################################
	
	## Flash Selection:	
	print
	print "-> Set flash controlled by ipbus"
	glib.getNode("ctrl_sram.flash_select").write(1);glib.dispatch()
        v=glib.getNode("ctrl_sram.flash_select").read();glib.dispatch()
	print "-> Flash_select pin is : ", v.value()	
	
	## Asynchronous read mode configuration
	print "-> "
	# print confAsyncRead(glib)
	confAsyncRead(glib)
	
		
	#################################################
	## Read MCS file and write User image to FLASH ##
	#################################################
	
	not_eof = 1		
	flash_block = []
	first_block = 1
	data_address = 0x0000
	write_buffer = []				
	
	area_error = 0
	
	start_time = time()
	
	mcs_file = open(args[1], 'r')	
	
	print "-> " 
	print "-> Parsing the User PROM file and writing the user FLASH region..."			
	print "-> "
	print "-> User PROM file:", mcs_file.name			
	
	while not_eof == 1:				
		
		mcs_line = mcs_file.readline()
		
		mcs_record_delimiter   = mcs_line[0]  # :
		mcs_record_data_length = mcs_line[1:3] 
		mcs_record_address     = mcs_line[3:7]
		mcs_record_type	       = mcs_line[7:9]
		
		mcs_ela_address	       = mcs_line[9:13]			
		
		######################################################
		# print "->"
		# print "-> mcs_line:", mcs_line[:((len(mcs_line)-1))] ## (len(mcs_line)-1) to avoid the end line character				
		######################################################
		
		if mcs_record_type == "04": ## ELA				
			
			ela	= int(mcs_ela_address, 16)				## Every ELA increment is FFFFh (64 KB)(32 KW)
			block_decrease = ela // ela_per_block		## Every Std Block of the FLASH is FFFFh (64 KW)(128 KB)
			first_ela_in_block = ela % ela_per_block 	## (0: first ela | 1: second ela)
			block_number = higher_user_block - block_decrease	
			
			if block_number > higher_user_block:
				
				print
				print "-> Error!!! PROM file has tried to write out of the User image area."
				print
				area_error = 1
				break
			
			prev_flash_block = flash_block
			flash_block = (higher_block-block_number)*0x10000 + glib.getNode("flash_block").getAddress()
			
			if first_ela_in_block == 0:	
				
				print "-> "
				print "-> Writing block %X..." % flash_block					
				
				if first_block == 0:
					# print "-> "
					# print blockLockOrUnlock(glib, prev_flash_block, "l")
					blockLockOrUnlock(glib, prev_flash_block, "l")
				first_block = 0
				
				unlock_and_erase_block = 1				
			
			############################################################
			# print "-> "
			# print "-> mcs_record_type:", mcs_record_type				
			# print "-> ela:", hex(ela)
			# print "-> block_decrease:", block_decrease
			# print "-> first_ela_in_block:", first_ela_in_block
			# print "-> flash_block:", flash_block
			# print "-> unlock_and_erase_block:", unlock_and_erase_block
			############################################################				
			
		elif mcs_record_type == "00": ## Data record								
			
			## Obtain the base address for fhe next burst write transfer from data record
			## stored on the first position of the buffer:				
			
			##################################################
			# print "->"
			# print "-> len(write_buffer):", len(write_buffer)				
			##################################################								
			
			if len(write_buffer) == 0:
				## Address is divided by two because the record is addressing Bytes while
				## the FLASH is addressed by words.
				##
				## It is also necessary to add the ela bit of the block before the division:
				data_address = ((first_ela_in_block*(2**16))+int(mcs_record_address, 16)) / 2
				
				#############################################
				# print "->"
				# print "-> Loading address..."									
				# print "-> data address:", hex(data_address)
				#############################################
			
			## Fills the write buffer with the data from the record (word by word):
			data_length = int(mcs_record_data_length, 16) ## Bytes								
			for i in range(data_length/2): ## Words
				mcs_data_word = mcs_line[(9+(4*i)):(9+((4*i)+4))]
				## Data bytes in the word must be swapped:
				lsB = mcs_data_word[0:2]
				msB	= mcs_data_word[2:4]
				swapped_word = msB + lsB
				## Data must be and integer:
				write_buffer.append(int(swapped_word, 16))									
			
			## Unlocks and erases when first access to a block:				
			if unlock_and_erase_block == 1:
				
				## Unlocks the block:
				# print "-> "
				# print blockLockOrUnlock(glib, flash_block, "u")
				blockLockOrUnlock(glib, flash_block, "u")
				
				## Erases the block:
				# print "-> "
				# print blockErase(glib, flash_block)
				blockErase(glib, flash_block)
				
				## Disables the unlock and erase procedure:
				unlock_and_erase_block = 0									
			
			## If the buffer is full writes performs the burst write transaction (64 Bytes) (32 Words):
			if len(write_buffer) == 32:							
				
				# print "-> "
				# print "-> Writing %X..." % flash_block								
				# print bufferProgram(glib, flash_block, data_address, write_buffer, max_write_buffer)											
				bufferProgram(glib, flash_block, data_address, write_buffer, max_write_buffer)
				
				## Deletes and declares again the buffer after write:
				del write_buffer
				write_buffer = []					
	
		elif mcs_record_type == "01": ## EOF				
			
			## If buffer is not empty write the flash:
			if len(write_buffer) != 0:
									
				# print "->"
				# print "-> Writing buffer remains:", len(write_buffer), "words"						
				# print bufferProgram(glib, flash_block, data_address, write_buffer, len(write_buffer))																
				bufferProgram(glib, flash_block, data_address, write_buffer, len(write_buffer))
			
			## Exits the while loop:
			not_eof = 0
		
		else:
		
			print
			print "-> Error!!! Unable to identify the record type..."
			print "-> **(The format of the PROM file must be MCS)** "
			print
			break
		
	mcs_file.close()
	
	if area_error == 0:
		elapsed_time = time() - start_time
		minutes = (elapsed_time // 60)
		seconds = elapsed_time % 60							
			
		print "->"	
		print "->  FLASH programming done."				
		print "->"	
		print "-> Process time: %d minutes and %d seconds." % (minutes, seconds)
		print
	
		print
		print
                glib.getNode("ctrl.fpga_program_b_trst").write(0);glib.dispatch()


	
else:

	print
	print "-> Error!!! one argument is required."
	print "-> "
	print "-> Syntax:"
	print "-> "
	print "-> glib_flash_user_prom_wr.py <file.mcs> "
	print "-> "	
	print "-> <file.mcs> - File to read"	
	print
	
