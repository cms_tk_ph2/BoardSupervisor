import FWCore.ParameterSet.python.Config as cms
import sys

if len(sys.argv)<4:
  print "Read StorageManager data files\n"
  print "Usage: cmsRun checkFileContents.py <input file> <output file>"
  sys.exit()

print "Input file: "+sys.argv[-2]
print "Output file: "+sys.argv[-1]


process = cms.Process("ContentTester")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.source = cms.Source("NewEventStreamFileReader",
                            #fileNames = cms.untracked.vstring('file:../db/closed/kab.812191607.0007.A.storageManager.00.0000.dat')
    			    #fileNames = cms.untracked.vstring('file:/home/echabert/USC.00000001.0001.A.storageManager.59.0000.dat')
    			    fileNames = cms.untracked.vstring('file:'+sys.argv[-2])
                            )

process.contentAna = cms.EDAnalyzer("EventContentAnalyzer")

process.FEDDump = cms.EDAnalyzer("DumpFEDRawDataProduct",
   label = cms.untracked.string("rawDataCollector"),
   feds = cms.untracked.vint32( 10,50, 1023 ),
   dumpPayload = cms.untracked.bool( True )
)

#process.poolDBESSource = cms.ESSource("PoolDBESSource",
#   BlobStreamerName = cms.untracked.string('TBufferBlobStreamingService'),
#   DBParameters = cms.PSet(
#        messageLevel = cms.untracked.int32(2),
#        authenticationPath = cms.untracked.string('/afs/cern.ch/cms/DB/conddb')
#    ),
#    timetype = cms.untracked.string('runnumber'),
#    connect = cms.string('sqlite_file:dbfile.db'),
#    toGet = cms.VPSet(
#    cms.PSet(record = cms.string('SiStripFedCablingRcd'),tag = cms.string('SiStripFedCabling_30X')),
#    cms.PSet(record = cms.string('SiStripNoiseRcd'),tag = cms.string('SiStripNoise_Fake_PeakMode_30X'))
#                      )
#)

import copy
from EventFilter.SiStripRawToDigi.python.SiStripRawToDigis_standard_cff import SiStripRawToDigis #as process.SiStripRawToDigis
#process.SiStripRawToDigis = SiStripRawToDigis
#siPixelDigis.InputLabel = 'rawDataCollector'
process.load("EventFilter.SiStripRawToDigi.python.SiStripRawToDigis_standard_cff")
process.siStripDigis.ProductLabel = 'rawDataCollector'


process.out = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string(sys.argv[-1]),
    SelectEvents = cms.untracked.PSet(
        SelectEvents = cms.vstring('p')
    ),    
    outputCommands = cms.untracked.vstring('keep *'),
    splitLevel = cms.untracked.int32(99),
    overrideInputFileSplitLevels = cms.untracked.bool(True),
    dropMetaData = cms.untracked.string('DROPPED')
)


#process.p = cms.Path(process.contentAna+process.FEDDump+process.SiStripRawToDigis)
process.p = cms.Path(process.contentAna+process.FEDDump)#+process.SiStripRawToDigis)

process.outpath = cms.EndPath(process.out)

