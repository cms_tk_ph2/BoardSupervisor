/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		Acquisition.h
   Content : 		CDR controller module
   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation : 01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#ifndef ACQUISITION_H_
#define ACQUISITION_H_

#include <string>
#include <time.h>
#include <fstream>
#include <boost/thread.hpp>
//#include <boost/chrono/thread_clock.hpp>
#include "xdaq/WebApplication.h"
#include "Utils/Visitor.h"
#include "FRLA.h"
#include "ParameterSet.h"
#include "CommissioningLoop.h"

#define NB_SIMULATION_BYTES	36

//Parameters written into board (used in default value, write into board, read from board, set from GUI, display in GUI) 
///Set True number of data packets
#define PARAM_DATASIZE 					"pc_commands.CBC_DATA_PACKET_NUMBER"
/// get Acquisition mode parameter. If false, controlled by L1A and data size must be 1 packet.
///Is trigger given by TTC (default) or internal
#define PARAM_TRIGGER_FROM_TTC 				"pc_commands.TRIGGER_SEL"
#define PARAM_TRIGGER_FREQ 				"pc_commands.INT_TRIGGER_FREQ"
///Is data from CBC ?
#define PARAM_EXTERNAL_DATA 				"pc_commands.CBC_DATA_GENE"
///Is clock signal shifted by 180 degrees ?
#define PARAM_CLOCK_SHIFT 				"pc_commands2.clock_shift"
#define PARAM_FE0_MASKED 				"pc_commands2.FE0_masked"
#define PARAM_FE1_MASKED 				"pc_commands2.FE1_masked"
#define PARAM_COMMISSIONNING_RQ 			"COMMISSIONNING_MODE_RQ"
#define PARAM_COMMISSIONNING_DELAY_AFTER_FAST_RESET 	"COMMISSIONNING_MODE_DELAY_AFTER_FAST_RESET"
#define PARAM_COMMISSIONNING_DELAY_AFTER_TEST_PULSE 	"COMMISSIONNING_MODE_DELAY_AFTER_TEST_PULSE"
#define PARAM_COMMISSIONNING_DELAY_AFTER_L1A 		"COMMISSIONNING_MODE_DELAY_AFTER_L1A"
#define PARAM_COMMISSIONNING_CBC_TEST_PULSE_VALID 	"COMMISSIONNING_MODE_CBC_TEST_PULSE_VALID"
#define PARAM_CBC_STUBDATA_LATENCY_ADJUST_FE0 		"cbc_stubdata_latency_adjust_fe1"
#define PARAM_CBC_STUBDATA_LATENCY_ADJUST_FE1 		"cbc_stubdata_latency_adjust_fe2"
#define PARAM_DIO5_THRESHOLD_TRIG_IN			"dio5.fmcdio5_threshold_trig_in"
#define PARAM_DIO5_THRESHOLD_CLK_IN			"dio5.fmcdio5_threshold_clk_in"
#define PARAM_DIO5_TRIG_IN_50OHMS			"dio5.fmcdio5_trig_in_50ohms"
#define PARAM_DIO5_TRIG_OUT_50OHMS			"dio5.fmcdio5_trig_out_50ohms"
#define PARAM_DIO5_CLK_IN_50OHMS			"dio5.fmcdio5_clk_in_50ohms"
#define PARAM_DIO5_CLK_OUT_50OHMS			"dio5.fmcdio5_clk_out_50ohms"
#define PARAM_DIO5_BACKPRESSURE_OUT_50OHMS 		"dio5.fmcdio5_backpressure_out_50ohms"
#define PARAM_DIO5_TRIG_IN_EDGE				"dio5.fmcdio5_trig_in_edge"	
#define PARAM_CLK_MUX_SEL				"dio5.clk_mux_sel"
#define PARAM_DIO5_BACKPRESSURE_OUT_POLAR		"dio5.fmcdio5_backpressure_out_polar"
#define PARAM_I2C_PRESCALER				"cbc_i2c_clk_prescaler"

#define STREAMER_SHORT_PAUSE 			"shortPause"
#define STREAMER_LONG_PAUSE 			"longPause"
#define STREAMER_USE_HARDWARE_COUNTER 	"hardwareCounter"
#define STREAMER_DISPLAY_COUNTERS		"displayCounters"
#define STREAMER_SHARED_MEMORY			"sharedMem"
#define STREAMER_MEMTOFILE				"memToFile"
//#define STREAMER_SIMULATED_DATA			"simulatedData"
#define STREAMER_NB_ACQ					"nbAcq"
#define STREAMER_DISPLAY_LOG			"log"
#define STREAMER_DISPLAY_FLAGS			"flags"
#define STREAMER_DISPLAY_DATA_FLAGS		"dataflags"
#define STREAMER_DATA_FILE				"dataFile"
#define STREAMER_DEST_FILE				"destination"
//#define STREAMER_DEST_MAX_SIZE				"destMaxSize"
//#define STREAMER_2ND_DEST_FILE				"secondDest"
//#define STREAMER_2ND_DEST_RATIO				"secondRatio"
#define STREAMER_NEW_DAQ_FILE			"NewDaqFile"
#define STREAMER_ZERO_SUPPRESSED		"zeroSuppressed"
#define STREAMER_CONDITION_DATA			"conditionData"
#define STREAMER_COMMISSIONING_LOOP		"commissioningLoop"
#define STREAMER_FAKE_DATA			"fakeData"
#define STREAMER_ACF_ACQUISITION		"middleware"
#define STREAMER_READ_NEXT_FILES		"readNextFiles"
#define STREAMER_BREAK_TRIGGER			"breakTrigger"
#define STREAMER_DATA_VISU			"dataVisu"
#define STREAMER_ACQ_MODE			"acqMode"
#define STREAMER_ACQ_MODE_FULLDEBUG		1		
#define STREAMER_ACQ_MODE_CBCERROR		2		
#define STREAMER_ACQ_MODE_SUMMARYERROR	0
#define STREAMER_ACQ_MODE_OLD			3		

#define CONDITION_DATA_ENABLED			"enabled_%02d"
#define CONDITION_DATA_FE_ID			"FE_ID_%02d"
#define CONDITION_DATA_CBC				"CBC_number_%02d"
#define CONDITION_DATA_PAGE				"page_number_%02d"
#define CONDITION_DATA_REGISTER			"I2C_register_%02d"
#define CONDITION_DATA_TYPE				"data_type_%02d"
#define CONDITION_DATA_VALUE			"value_%02d"
#define CONDITION_DATA_NAME			"name_%02d"
#define NB_CONDITION_DATA			5

#define CONFIG_NB_FE				"nb_FE"
#define CONFIG_HYBRID_TYPE			"hybrid_type"
#define CONFIG_HYBRID_VERSION		"new.hybrid_version"
#define CONFIG_CARRIER_VERSION		"new.carrier_version"
#define CONFIG_MEZZANINE_VERSION	"new.mezzanine_version"
#define CONFIG_FE_HEADER			"FE%d"
#define CONFIG_FE_ENABLED			"FE%d.enabled"
#define CONFIG_FE_CBC_ENABLED		"FE%d.CBC%d"

#define TEXT_EVENT_APPROXIMATE_SIZE	740

//#include <linux/types.h>
namespace Ph2_HwInterface{ class BeBoardInterface;}
namespace Ph2_HwInterface{ class CbcInterface;}
namespace Ph2_HwInterface{ class Event;}
namespace Ph2_HwDescription{ class BeBoard;}
class FileHandler;

class Acquisition;
/*
class AcfVisitor: public HwInterfaceVisitor{
	Acquisition* pAcquisition;
	std::ofstream *pfilSave, *pfilSave2nd;
	uint64_t luMaxSize, luDestSize, lu2ndDestSize;
	uint32_t uRatio2nd, uNumAcq;
public:
	void init(std::ofstream* pfSave, std::ofstream* pfSave2nd, Acquisition* pAcq);
	virtual void visit ( const Ph2_HwInterface::Event& pEvent );
};*/
///Data acquisition class to start and stop acquisitions
class Acquisition
{
public:
	Acquisition();
	virtual ~Acquisition();
	///Is the acquisition currently running ?
	bool isRunning() ;
	///Is the acquisition currently saving data to file ?
	bool isSavingData() const;
	///Is destination file descriptor open ?
	bool isDestFileOpen() const ;
	///Is second destination file descriptor open ?
	//bool is2ndDestFileOpen() const { return filSave2nd.is_open();}
	///Is New DAQ format file descriptor open ?
	bool isDaqFileOpen() const { return filNewDaq.is_open();}
	bool isDataFileOpen() const;
	///Is the destination file in text format ?
	bool isTextDestination() const { return textDestination; }
	///Is the second destination file in text format ?
	bool isText2ndDestination() const { return text2ndDestination; }
	/**Read a counter once and once more after 1 s to calculate board clock frequency.
	 * @return difference between the 2 values */
	uint32_t frequenceMetre();
	///Get data acquisition number (one for each CBC data)
	uint32_t getNumAcq();
	uint32_t getNumCommissioningLoop() const {return nbCommMax-nbCommAcq+1;}
	uint32_t getNbCommissioningLoop() const {return nbCommMax;}
	///Set the destination file path where acquisition data is saved
//	void setDestinationFile(const std::string& strFile);
	///Set the file path to read simulated data from
//	void setSimulatedDataFile(const std::string& strFile);
	///Set the destination file path where acquisition data is packed into the new format
//	void setNewDaqFile(const std::string& strFile);
	///Set the uhal hardware interface (GLIB board) running IPBus.
	void setBoard( Ph2_HwInterface::BeBoardInterface* pbbi, Ph2_HwInterface::CbcInterface *pcbci, Ph2_HwDescription::BeBoard* pbb);
	/// GLIB configuration method called by BoardSupervisor
	void configure(bool bLog);
	///Run an acquisition in its own execution thread.
	void run();
	///Stop an acquisition (if it is running) and wait for the thread to be finished.
	void stop();
	
	void togglePause();
	bool isPaused() const {return paused;}
	bool isPauseToggling() const {return pauseToggle;}
//	void breakTriggers(bool bBreak);
	///Get acquisition log.
	const std::string& getLog() const{return strLog;}
	///Get last time the acquisition was started using force_BG0 signal
	const std::string& getLastStartTime() const{return lastStartTime;}
	///Get destination file path where acquisition raw data is saved
//	const std::string& getNewDaqFile() const {return newDaqFile;}
	///Get destination file path where acquisition raw data is saved
//	const std::string& getDestinationFile() const {return destFile;}
	///Get file path to read the simulated data from
//	const std::string& getSimulatedDataFile() const {return simulatedDataFile;}
	///Get file path where memory content sent to RU-BU-FU will also be dumped
	std::string getMemDumpFilename() const;
	///Transfer rate in bits/s computed at the end of the last acquisition
	double getTransferRate() const {return transferRate;}
	double getTriggerRate() const {return triggerRate;}
	double getTriggerRateInstant() const {return triggerRateInstant;}
	///Set the duration (in ms) of the pause after initialisation and configuration
//	void setLongPause(uint32_t uPause);
	///duration (in ms) of the pause after initialisation and configuration
//	uint32_t getLongPause() const {return longPause;}
	///Set the duration (in ms) of the short pause to wait for CBC_DATA_READY flag
//	void setShortPause(uint32_t uPause);
	///duration (in ms) of the short pause to wait for CBC_DATA_READY flag
//	uint32_t getShortPause() const {return shortPause;}
	///Are flags to be displayed during acquisition
//	bool getDisplayFlags() const {return displayFlags;}
	///Tell if flags have to be displayed in acquisition description
//	void setDisplayFlags(bool bFlags);
	///Value of status flags
	uint32_t getStatusFlags() const {return statusFlags;}
	///Are acquisition counters to be displayed
//	bool getDisplayCounters() const {return displayCounters;}
	///Tell if acquisition counters have to be displayed
//	void setDisplayCounters(bool bDisplay);
	///Is acquisition log to be displayed
//	bool getDisplayLog() const {return displayLog;}
	///display acquisition log or not
//	void setDisplayLog(bool bLog);
	///Start a new acquisition log
	void initLog(const std::string& strInit);
	///Are data flags displayed ?
//	bool getDisplayDataFlags() const {return displayDataFlags;}
	///Set if data flags will be displayed
//	void setDisplayDataFlags(bool bDisplay);
	///Get data flags (sTTS_code, Spurious_frame_detect, cdr_clk_locked_detect, cdr_los_detect, cdr_lol_detect)
	uint32_t getDataFlags() const {return dataFlags;}
	/// Is acquisition data simulated
//	bool getSimulatedData() const {return simulatedData;}
	/// Set if acquisition data is simulated 
//	void setSimulatedData(bool bSimu);
	/// Set if event counter is given by hardware (or incremented by software) 
//	void setUseHardwareCounter(bool bHard);
	/// Is event counted given by hardware ?
//	bool getUseHardwareCounter() const {return useHardwareCounter;}
	/// Simulation data pointer (entered simulated data)
	char* getSimulationArray(){return simulationData;}
	/// Is generated memory content dumped to file 
//	bool getMemToFile() const {return memToFile;}
	/// Set if content sent to shared memory is also dumped into a file (path given by getMemDumpFilename()) 
//	void setMemToFile(bool bFile);
	///Number of acquisition cycles to be performed
//	uint32_t getNbAcq() const {return nbAcq;}
	///Set number of acquisition cycles to be performed
//	void setNbAcq(uint32_t uNb);
	///CBC reset selection
	uint32_t getResetSelection() const { return resetSelection;}
	void setResetSelection(uint32_t uSel);
	uint32_t getTriggerMode() const { return triggerMode;}
	void setTriggerMode(uint32_t uMode);
	uint32_t getTriggerCyclicFrequency() const { return triggerCyclicFrequency;}
	void setTriggerCyclicFrequency(uint32_t uFreq);
	///BoardSupervisor acquisition parameters set from the board
	ParameterSetUhal& getParameterSet() {return pSetSuper;}
	///GlibStreamer parameter set
	ParameterSetUhal& getStreamerParameterSet() {return pSetStreamer;}
	///GlibStreamer condition data parameter set
	ParameterSetUhal& getConditionParameterSet() {return pSetCondition;}
	///Front End and CBC configuration parameter set
	ParameterSetUhal& getConfigParameterSet() {return pSetConfig;}
	///Front End and CBC enabling parameter set
	ParameterSetUhal& getFeCbcParameterSet() {return pSetFeCbc;}
	///Commissioning Loop Object
	CommissioningLoop& getCommissioningLoop() {return comLoop;}
	
	/// get read unit descriptor where data will be sent via shared memory
	xdaq::ApplicationDescriptor * getReadUnitDescriptor() const{return ruDescriptor_;}
	/// Send a spurious frame to board
	void sendSpuriousFrame();
	/// Send a Trigger one shot pulse
	void sendTriggerOneShot();
	///Read configuration parameters values from board
	void initConfiguration();
	/// Load status flags from board
	void loadFlags();
	/** Set shared memory parameters
	 * @param mem Pointer to the RU buffer pool (FRLA) 
	 * @param ruDesc Read Unit application descriptor. if NULL, memory content is dumped to a temporary file
	 * @param app XDAQ application
	 */
	void setSharedMemory(FRLA *mem, xdaq::ApplicationDescriptor *ruDesc, xdaq::Application* app);
	/// Get shared memory object
	FRLA* getSharedMemory() const {return sharedMem;}
	/// Force start by setting force_BG0 signal during acquisition
	void forceStart();
	/// Stop saving acquisition raw data to file
	void stopSaving();
	/// Restart saving acquisition data into a new file
	void startSaving();
	///Front End and CBC enabling parameter set initialisation
	void initFeCbcParameterSet();
	uint64_t getEnabledFrontEnd(ParameterSetUhal& pSetConfig, ParameterSetUhal& pSetFeCbc);
	///Close the file, increment its file name until a non-existing file name is found and create the new file.
	void incrementDestinationFile(std::ofstream& filDest, const std::string& strParam);
	/** Close the file, increment its file name and open the new file.
	 * @return True if a new file could be open */ 
	bool incrementInputFile(std::fstream&  filInput, const std::string& strParam);
	/// Description of acquisition counters as HTML table
	std::string getCountersAsHtml(const char* small);
	uint32_t* getDataVisu() const { return arrVisu;}
	void setFwVersion(uint32_t uVer);
	/** Store visualisation data for the current commissioning loop */
	void storeVisuData(uint32_t* arrData);
	std::string getDataVisuCommLoopString(uint32_t iCom);
	uint32_t* getDataVisuCommLoop(uint32_t iCom);
	void setAutoExit(bool bExit) {bAutoExit = bExit;}
private:
	std::list<toolbox::mem::Reference*> lstRef; 
	bool running, paused, pauseToggle;
//	bool displayFlags, displayCounters, displayLog, displayDataFlags;
//	bool simulatedData;
	bool sendForceStart, spuriousFrame; 
	bool binaryRawSimulation, binaryDaqSimulation;
	bool textDestination, text2ndDestination;
	bool bAutoExit;
	uint32_t triggerOneShot;
	uint32_t numAcq, software_event_counter;
	uint32_t statusFlags, dataFlags;
	uint32_t nbBunchCrossing, nbOrbit, nbLumiSection, nbL1A, nbCbcData;
	uint32_t resetSelection, triggerMode, triggerCyclicFrequency;
	uint32_t nbCBC, uPacketSize, uFwVersion;
	uint32_t nbMaxAcq, nbCommAcq, nbCommMax;
	uint32_t *arrVisu;
	Ph2_HwInterface::Event *pEventCopy, *pEventCopy2;
	uint64_t luDestFileSize, lu2ndDestFileSize, luDaqFileSize, luEvtCounter;
	double transferRate, triggerRate, triggerRateInstant;
	timeval logStart, logEnd, acqStart, timForceStart;
//	std::string destFile, newDaqFile;
//	std::string simulatedDataFile;
	std::string strLog;
	std::string lastStartTime;
	std::ofstream filMemDump, filNewDaq;//filSave2nd,
//	std::ifstream filSimulatedData;
	FileHandler *pfilSave, *pfilSimulatedData;
	//uhal::HwInterface *board;
	Ph2_HwInterface::BeBoardInterface* beBoardInterface;                     /*!< Interface to the BeBoard */
	Ph2_HwInterface::CbcInterface* cbcInterface;    
	Ph2_HwDescription::BeBoard* beBoard;
	ParameterSetUhal pSetSuper, pSetStreamer, pSetCondition, pSetConfig, pSetFeCbc;
	CommissioningLoop comLoop;
	boost::thread thrAcq;
	FRLA *sharedMem;
	xdaq::ApplicationDescriptor *ruDescriptor_;
	xdaq::Application* application_;
	char simulationData[NB_SIMULATION_BYTES];
	//AcfVisitor visitor;
	
	/** Method executed in a separate thread to perform data acquisition.
	 * @param bVerif Check that the data is AAAAAAAA 55555555... and display error messages in log if it is not.
	 * @param bLog A log will be generated for the acquisition
	 */
	void start(bool bVerif, bool bLog);
	void startAcf(bool bFlags, bool bLog);
	///Append one line to the acquisition log
	void appendLog(const std::string& strAppend);
	void saveData(std::vector< uint32_t > lData, char* bufWrite, bool bLog, uint32_t nbPackets);
	void checkData(std::vector< uint32_t > lData, bool bVerif, bool *pb5555, uint32_t *pNbErr);
	void sendData(const char * buffer, uint32_t index, bool bLog);
	bool readSimulatedDataFromFile(char* buffer, uint32_t uPack);
	bool readSimulatedDataFromTextFile(char* buffer);
	void closeFiles();
	void readI2CValuesForConditionData();
	uint32_t nbTotalCbc();
	void addDataVisu(const Ph2_HwInterface::Event* pEvent, bool bLog);
	void autoExit(double dblDur);
};

#endif /*ACQUISITION_H_*/
