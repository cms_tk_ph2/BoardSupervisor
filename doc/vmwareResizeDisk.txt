Augmenter la taille du disque d'une machine virtuelle Linux sous VMWare

Tutoriel : 
http://kb.vmware.com/selfservice/microsites/search.do?cmd=displayKC&docType=kc&docTypeID=DT_KB_1_1&externalId=1006371

1. Power off the virtual machine.
2. Edit the virtual machine settings and extend the virtual disk size:
	Settings, HardwareHard Disk, Utilities, Expand
3. Power on the virtual machine.
4. Identify the device name (/dev/sda): 
	# /sbin/fdisk -l
5. Create a new primary partition:
	Run the command: # /sbin/fdisk /dev/sda (depending the results of the step 4) 
	Press p to print the partition table to identify the number of partitions. By default there are 2: sda1 and sda2.
	Press n to create a new primary partition. 
	Press p for primary.
	Press 3 for the partition number, depending the output of the partition table print.
	Press Enter two times.
	Press w to write the changes to the partition table.
6. Restart the virtual machine.
7. Run this command to verify that the changes were saved to the partition table and that the new partition has an 83 type:
	# fdisk -l
8. Run this command to convert the new partition to a physical volume:
	# pvcreate /dev/sda3
9. Run this command to extend the physical volume:
	# vgextend VolGroup00 /dev/sda3
Note: To determine which volume group to extend, use the command vgdisplay.

10. Run this command to verify how many physical extents are available to the Volume Group:
	# vgdisplay VolGroup00 | grep "Free"

11. Run the following command to extend the Logical Volume:
	# lvextend -L+#G /dev/VolGroup00/LogVol00

Where # is the number of Free space in GB available as per the previous command.
Note: to determine which logical volume to extend, use the command lvdisplay.

12. Run the following command to expand the ext3 filesystem online, inside of the Logical Volume:
	# resize2fs /dev/VolGroup00/LogVol00

Note: Use ext2online instead of resize2fs if it is a Red Hat virtual machine.

13. Run the following command to verify that the / filesystem has the new space available:
	# df -h /
